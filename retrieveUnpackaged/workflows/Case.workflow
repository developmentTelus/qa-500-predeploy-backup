<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_addressed</fullName>
        <description>Email Notification: Your Case has been addressed</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service_2_0_Email_Templates/Service2dot0_EMAIL_CloseCaseEmail_EN</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_addressed_Cantonese</fullName>
        <description>Email Notification: Your Case has been addressed (Cantonese)</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service2_0_Email_Templates_Chinese/Service2dot0_EMAIL_CloseCaseEmail_TC_V1</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_addressed_Chinese</fullName>
        <description>Email Notification: Your Case has been addressed (Chinese)</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service2_0_Email_Templates_Chinese/S2dot0_EMAIL_CloseCaseEmail_ZHS_V2</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_addressed_French</fullName>
        <description>Email Notification: Your Case has been addressed (French)</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mod_les_de_courriel_du_S2_0_de_TELUS/Service2dot0_EMAIL_CloseCaseEmail_FR_V2</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_addressed_Mandarin</fullName>
        <description>Email Notification: Your Case has been addressed (Mandarin)</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service2_0_Email_Templates_Chinese/S2dot0_EMAIL_CloseCaseEmail_ZHS_V</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_created</fullName>
        <description>Email Notification: Your Case has been created</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service_2_0_Email_Templates/Service2dot0_EMAIL_FINAL_CaseRepIntro_EN</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_created_Cantonese</fullName>
        <description>Email Notification: Your Case has been created Cantonese</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service2_0_Email_Templates_Chinese/Service2dot0_EMAIL_CaseRepIntro_TC_V1</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_created_FR</fullName>
        <description>Email Notification: Your Case has been created FR</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mod_les_de_courriel_du_S2_0_de_TELUS/Service2dot0_EMAIL_CaseRepIntro_FR_V2</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_created_Mandarin</fullName>
        <description>Email Notification: Your Case has been created Mandarin</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service2_0_Email_Templates_Chinese/Service2dot0_EMAIL_CaseRepIntro_ZHS_V2_3</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Your_Case_has_been_created_ZHS</fullName>
        <description>Email Notification: Your Case has been created ZHS</description>
        <protected>false</protected>
        <recipients>
            <field>Notification_ID__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service2_0_Email_Templates_Chinese/Service2dot0_EMAIL_CaseRepIntro_ZHS_V2_2</template>
    </alerts>
    <alerts>
        <fullName>Email_Reminder_Field_Service_Appointment_in_2_days</fullName>
        <description>Email Reminder: Field Service Appointment in 2 days</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>salesforcecmt@telus.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TELUS_Service_2_0_Email_Templates/Service2dot0_EMAIL_Generic_EN_V2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Auto_Change_Record_Type_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Auto_Change Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_update_Completed_Timestamp</fullName>
        <field>Date_Time_Completed__c</field>
        <formula>NOW()</formula>
        <name>Auto-update Completed Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Can_Be_Reached_Phone_Update</fullName>
        <field>Can_Be_Reached_Phone__c</field>
        <formula>IF (NOT(REGEX(Can_Be_Reached__c, &quot;\\D*?(\\d\\D*?){10}&quot;)),&quot;&quot;,TRIM(Can_Be_Reached__c) )</formula>
        <name>Can_Be_Reached_Phone Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Hard_Close</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case Hard Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Soft_Close</fullName>
        <field>Status</field>
        <literalValue>Pending Close</literalValue>
        <name>Case Soft Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Closed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Change Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Auto_close</fullName>
        <field>Auto_Close__c</field>
        <literalValue>1</literalValue>
        <name>Check Auto-close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checkoff_1st_Queue_to_User</fullName>
        <field>X1st_time_trigger__c</field>
        <literalValue>1</literalValue>
        <name>Checkoff 1st Queue to User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_FirstAgentAssigned_Field</fullName>
        <field>FirstAgentAssigned__c</field>
        <formula>CASESAFEID( OwnerId)</formula>
        <name>Populate FirstAgentAssigned Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Open</fullName>
        <description>Replace &quot;Update Record Type to Open&quot; Workflow Field Update</description>
        <field>RecordTypeId</field>
        <lookupValue>Open_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BAN_for_Case</fullName>
        <field>BAN__c</field>
        <formula>Account.Billing_Account_Number__c</formula>
        <name>Update BAN for Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_FFH_Concierge_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>FFH_Concierge_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to FFH_Concierge_Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_FFH_GCM_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>FFH_GCM_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to FFH_GCM_Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_Mobility_EN</fullName>
        <field>OwnerId</field>
        <lookupValue>MOB_Activations_Reactive_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to Mobility - EN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_Timestamp</fullName>
        <field>Date_Time_Completed__c</field>
        <formula>DATETIMEVALUE(NOW())</formula>
        <name>Update Completed Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_FFH_Onboarding_Reactive</fullName>
        <field>OwnerId</field>
        <lookupValue>FFH_Onboarding_Reactive_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to FFH_Onboarding_Reactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_MOB_Renewals_Proactive</fullName>
        <field>OwnerId</field>
        <lookupValue>MOB_Renewals_Proactive_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to  MOB Renewals Proactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_RTS_Renewal_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MOB_Renewals_Reactive_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to RTS Renewal Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Repair_Reactive_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MOB_DeviceRepair_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Repair Reactive Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Completed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Open</fullName>
        <description>*DO NOT USE* Replaced by &quot;Record Type to Open&quot; Workflow Field Update.</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Update Record Type to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Pending_Close</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pending_Close</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Pending Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <flowActions>
        <fullName>Run_Create_Tasks_based_on_New_Case</fullName>
        <flow>Create_Tasks_based_on_New_Case</flow>
        <flowInputs>
            <name>CaseID</name>
            <value>{!Id}</value>
        </flowInputs>
        <label>Run Create Tasks based on New Case</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <flowActions>
        <fullName>Run_Update_Task_OwnerID_Flow</fullName>
        <flow>Update_Task_OwnerID</flow>
        <flowInputs>
            <name>CaseID</name>
            <value>{!Id}</value>
        </flowInputs>
        <label>Run Update Task OwnerID Flow</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>1st Time Trigger</fullName>
        <actions>
            <name>Checkoff_1st_Queue_to_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow captures the first time a case is transferred from a queue to a user or is created by a user to prevent duplicate tasks from being created</description>
        <formula>OR(   (CONTAINS(PRIORVALUE( OwnerId ) ,&quot;00G&quot;) &amp;&amp;  CONTAINS(OwnerId, &quot;005&quot;)) &amp;&amp; NOT(ISNEW()),   (NOT(Owner:User.Alias  =&quot;rtest&quot;) &amp;&amp; CONTAINS(OwnerId, &quot;005&quot;) &amp;&amp; ISNEW()) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration User1 Case Owner to FFH_Concierge_Queue</fullName>
        <actions>
            <name>Update_Case_Owner_to_FFH_Concierge_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>OMS-CONCIERGE</value>
        </criteriaItems>
        <description>Make change to the existing Workflow Rule. If the case comes from ETL and source type is OMS-Concierge , the case gets assigned to FFH_Concierge_Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration User1 Case Owner to FFH_GCM_Queue</fullName>
        <actions>
            <name>Update_Case_Owner_to_FFH_GCM_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>OMS-GCM</value>
        </criteriaItems>
        <description>Make change to the existing Workflow Rule. If the case comes from ETL and source type is OMS-GCM , the case gets assigned to FFH_GCM_Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration User1 Case Owner to RPT1-Activition En Queue</fullName>
        <actions>
            <name>Update_Case_Owner_to_Mobility_EN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>RPT1-ACTIVATION</value>
        </criteriaItems>
        <description>Make change to the existing Workflow Rule. If the case comes from ETL and source type is RPT1 - Activation, the case gets assigned to RP1- Activation EN queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration1 User to Device Repair Reactive EN Queue</fullName>
        <actions>
            <name>Update_Owner_to_Repair_Reactive_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>RTS-REPAIR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <description>Workflow rule created to assign all newly created cases to Device Repair queue if the case Source Type is rts-repair</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration1 User to FFH_Onboarding_Reactive_EN</fullName>
        <actions>
            <name>Update_Owner_to_FFH_Onboarding_Reactive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>OMS-ORDERS</value>
        </criteriaItems>
        <description>Description of Assign Integration1 User to FFH_Onboarding_Reactive_EN: Workflow rule created to assign all newly created cases to FFH_Onboarding_Reactive_EN Queue if the case Source Type is OMS-ORDER</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration1 User to MOB Renewals Proactive EN Queue</fullName>
        <actions>
            <name>Update_Owner_to_MOB_Renewals_Proactive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>LNR-RENEWAL</value>
        </criteriaItems>
        <description>Workflow rule created to assign all newly created cases to MOB Renewals Proactive EN Queue if the case Source Type is L&amp;R-Renewal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Integration1 User to Reactive Renewals Queue</fullName>
        <actions>
            <name>Update_Owner_to_RTS_Renewal_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Type__c</field>
            <operation>equals</operation>
            <value>RPT1-RENEWAL</value>
        </criteriaItems>
        <description>Workflow rule created to assign all newly created cases to Reactive Renewal Queues if the case Source Type is RPT1-RENEWAL</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Can Be Reached Phone Update</fullName>
        <actions>
            <name>Can_Be_Reached_Phone_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2)  AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Can_Be_Reached__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Can_Be_Reached__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.isCanBeReachedNull__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>CES20-68 - to update &quot;Can_Be_Reached_Phone__c&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Completed Notification</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_addressed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send an email notification to inform the customer that the case has been addressed</description>
        <formula>AND( ISPICKVAL(PRIORVALUE (Status),  &quot;In-Progress&quot;) , OR(ISPICKVAL(Status,&quot;Warranty&quot;),ISPICKVAL(Status,&quot;Closed&quot;)), ISPICKVAL(Notification__c ,&quot;Email&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Delivery Review</fullName>
        <actions>
            <name>Case_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In-Progress</value>
        </criteriaItems>
        <description>To remind the Case Rep to review the case when the case status is In-Progress and the Due Date has passed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type - Completed to Closed</fullName>
        <actions>
            <name>Change_Record_Type_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( ISPICKVAL( Status , &quot;Closed&quot;),  ISPICKVAL( Status , &quot;Cancelled&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type - Open to Completed</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Warranty</value>
        </criteriaItems>
        <description>Changes the case record type from Open Case to Completed when status changes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Review Checkbox</fullName>
        <actions>
            <name>Close_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Creates a task for the case rep to review the case if the Close Review checkbox is checked</description>
        <formula>OR (AND( Close_Review__c =TRUE, NOT( OwnerId =&quot;005o0000000LVQG&quot;), ISNEW()= TRUE), AND( Close_Review__c =TRUE,PRIORVALUE( OwnerId )=&quot;00Go0000000Ndbp&quot; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Relevant Tasks</fullName>
        <actions>
            <name>Run_Create_Tasks_based_on_New_Case</name>
            <type>FlowAction</type>
        </actions>
        <active>false</active>
        <description>If required, create:
- Order Review
- Pre-Appointment Review
- Courtesy Call
- Bill Review</description>
        <formula>OR (  AND( NOT( Owner:User.Alias  =&quot;rtest&quot;), NOT(CONTAINS(OwnerId,&quot;00G&quot;)), ISNEW()= TRUE,  X1st_time_trigger__c = FALSE  ),  AND( X1st_time_trigger__c = FALSE , NOT( Owner:User.Alias  =&quot;rtest&quot;), NOT(CONTAINS(OwnerId,&quot;00G&quot;)), CONTAINS(PRIORVALUE( OwnerId ),&quot;00G&quot;)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Service Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In-Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsFielded__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Email_Opt_Out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>To send an email reminder to the customer 2 days before a scheduled field service</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Field_Service_Appointment_in_2_days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Reminder_to_Customer_Field_Service_Appointment_in_2_days</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Due_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>First Case Rep Assigned</fullName>
        <actions>
            <name>Populate_FirstAgentAssigned_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To capture the name/ID of the case rep who was first assigned the case</description>
        <formula>AND( 
ISBLANK( FirstAgentAssigned__c )=TRUE, 
NOT(OR( 
CONTAINS( OwnerId ,&quot;00G&quot;), 
Owner:User.Alias =&quot;rtest&quot; 
)) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notification</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_created</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_created</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When a new case is created by the case rep, an email notification is sent to the customer.  Note, Userids that start with &apos;005&apos; refer to regular SFDC users; others will be skipped (e.g., queues)</description>
        <formula>AND(  ISPICKVAL(Status,&quot;Open&quot;),   ISPICKVAL(Notification__c,&quot;Email&quot;),    LEFT(OwnerId, 3) = &quot;005&quot;,  NOT(  Owner:User.Alias  =&quot;rtest&quot;),  OR(ISPICKVAL(Language_Preference__c,&quot;English&quot;),ISPICKVAL(Language_Preference__c,&quot;EN&quot;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notification %28Cantonese%29</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_created_Cantonese</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_created</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When a new case is created by the case rep, an email notification is sent to the customer (Cantonese)</description>
        <formula>AND(  ISPICKVAL(Status,&quot;Open&quot;),  ISPICKVAL(Notification__c,&quot;Email&quot;),  LEFT(OwnerId, 3) = &quot;005&quot;,  NOT(Owner:User.Alias  =&quot;rtest&quot;),  ISPICKVAL(Language_Preference__c,&quot;Cantonese&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notification %28Chinese%29</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_created_ZHS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_created</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>When a new case is created by the case rep, an email notification is sent to the customer (Chinese)</description>
        <formula>AND(  ISPICKVAL(Status,&quot;Open&quot;),  ISPICKVAL(Notification__c,&quot;Email&quot;),  LEFT(OwnerId, 3) = &quot;005&quot;,  NOT(Owner:User.Alias  =&quot;rtest&quot;),  ISPICKVAL(Language_Preference__c,&quot;Chinese&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notification %28French%29</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_created_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_created</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When a new case is created by the case rep, an email notification is sent to the customer (French)</description>
        <formula>AND(  ISPICKVAL(Status,&quot;Open&quot;),  ISPICKVAL(Notification__c,&quot;Email&quot;),  LEFT(OwnerId, 3) = &quot;005&quot;,  NOT(Owner:User.Alias  =&quot;rtest&quot;),  OR(ISPICKVAL( Language_Preference__c ,&quot;French&quot;),ISPICKVAL( Language_Preference__c ,&quot;FR&quot;) ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notification %28Mandarin%29</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_created_Mandarin</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_created</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When a new case is created by the case rep, an email notification is sent to the customer (Mandarin)</description>
        <formula>AND(  ISPICKVAL(Status,&quot;Open&quot;),  ISPICKVAL(Notification__c,&quot;Email&quot;),  LEFT(OwnerId, 3) = &quot;005&quot;,  NOT(Owner:User.Alias  =&quot;rtest&quot;),  ISPICKVAL(Language_Preference__c,&quot;Mandarin&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notification - Other Languages</fullName>
        <actions>
            <name>Email_Notification_Your_Case_has_been_created</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Customer_Your_Case_has_been_created</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When a new case is created by the case rep, an email notification is sent to the customer.  Note, Userids that start with &apos;005&apos; refer to regular SFDC users; others will be skipped (e.g., queues)</description>
        <formula>AND( ISPICKVAL(Status,&quot;Open&quot;),   ISPICKVAL(Notification__c,&quot;Email&quot;),    LEFT(OwnerId, 3) = &quot;005&quot;,  NOT(  Owner:User.Alias  =&quot;rtest&quot;),  AND (NOT ISPICKVAL(Language_Preference__c,&quot;English&quot;), NOT ISPICKVAL(Language_Preference__c,&quot;EN&quot;), NOT ISPICKVAL(Language_Preference__c,&quot;FR&quot;), NOT ISPICKVAL(Language_Preference__c,&quot;French&quot;), NOT ISPICKVAL(Language_Preference__c,&quot;Mandarin&quot;), NOT ISPICKVAL(Language_Preference__c,&quot;Cantonese&quot;) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order Review Checkbox</fullName>
        <actions>
            <name>Order_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Order_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Creates a task for the case rep to review the case if the Order Review checkbox is checked</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Outcome Review Checkbox</fullName>
        <actions>
            <name>Outcome_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Creates a task for the case rep to review the case if the Outcome Review checkbox is checked</description>
        <formula>OR (AND( Outcome_Review__c =TRUE, NOT( OwnerId =&quot;005o0000000LVQG&quot;), ISNEW()= TRUE), AND( Outcome_Review__c =TRUE,PRIORVALUE( OwnerId )=&quot;00Go0000000Ndbp&quot; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pending Close to Open</fullName>
        <actions>
            <name>Record_Type_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ensure that record type changes when status changes from Pending Close to Open</description>
        <formula>ISPICKVAL(Status,&quot;Open&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pending Close workflow</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Triggers email alert and field update on the 9th and 12th days respectively</description>
        <formula>AND(ISPICKVAL(Status,&quot;Pending Close&quot;),ISPICKVAL(Notification__c,&quot;Email&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow - Cantonese</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers email alert and field update on the 4th and 7th days respectively
Sends Cantonese Email template</description>
        <formula>AND( ISPICKVAL(Status,&quot;Pending Close&quot;), ISPICKVAL(Notification__c,&quot;Email&quot;), ISPICKVAL(Language_Preference__c,&quot;Cantonese&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed_Cantonese</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow - Chinese</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Triggers email alert and field update on the 4th and 7th days respectively
Sends Chinese Email template</description>
        <formula>AND( ISPICKVAL(Status,&quot;Pending Close&quot;), ISPICKVAL(Notification__c,&quot;Email&quot;), ISPICKVAL(Language_Preference__c,&quot;Chinese&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed_Chinese</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow - French</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers email alert and field update on the 4th and 7th days respectively. 
Sends French email template.</description>
        <formula>AND(ISPICKVAL(Status,&quot;Pending Close&quot;),ISPICKVAL(Notification__c,&quot;Email&quot;),  OR(ISPICKVAL(Language_Preference__c,&quot;French&quot;),ISPICKVAL(Language_Preference__c,&quot;FR&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed_French</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow - Mandarin</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers email alert and field update on the 4th and 7th days respectively
Sends Mandarin Email template</description>
        <formula>AND( ISPICKVAL(Status,&quot;Pending Close&quot;), ISPICKVAL(Notification__c,&quot;Email&quot;), ISPICKVAL(Language_Preference__c,&quot;Mandarin&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed_Mandarin</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow - Other Languages</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers email alert and field update on the 4th and 7th days respectively for all Other Languages - NOT English , French , Mandarin or Cantonese Language Preference</description>
        <formula>AND( ISPICKVAL(Status,&quot;Pending Close&quot;), ISPICKVAL(Notification__c,&quot;Email&quot;),  AND (NOT ISPICKVAL(Language_Preference__c,&quot;English&quot;) ,NOT ISPICKVAL(Language_Preference__c,&quot;EN&quot;) ,NOT ISPICKVAL(Language_Preference__c,&quot;FR&quot;) ,NOT ISPICKVAL(Language_Preference__c,&quot;French&quot;) ,NOT ISPICKVAL(Language_Preference__c,&quot;Mandarin&quot;) ,NOT ISPICKVAL(Language_Preference__c,&quot;Cantonese&quot;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow V2</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers email alert and field update on the 4th and 7th days respectively</description>
        <formula>AND( ISPICKVAL(Status,&quot;Pending Close&quot;), ISPICKVAL(Notification__c,&quot;Email&quot;), OR(ISPICKVAL(Language_Preference__c,&quot;English&quot;),ISPICKVAL(Language_Preference__c,&quot;EN&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Your_Case_has_been_addressed</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Notification_to_Customer_Your_Case_has_been_addressed</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow w%2Fo email</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Triggers field update on the 12th day</description>
        <formula>AND(ISPICKVAL(Status,&quot;Pending Close&quot;),ISPICKVAL(Notification__c,&quot;None&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Close workflow w%2Fo email V2</fullName>
        <actions>
            <name>Update_Record_Type_to_Pending_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers field update on the 7th day</description>
        <formula>AND(ISPICKVAL(Status,&quot;Pending Close&quot;),ISPICKVAL(Notification__c,&quot;None&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_Record_Type_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Change_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Check_Auto_close</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate BAN %23 for a Case%2E</fullName>
        <actions>
            <name>Update_BAN_for_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow rule Updates BAN__c field on Case with the Account Billing Account Number in order for the case to appear on Global Search : JIRA # CES20-294.</description>
        <formula>OR(ISCHANGED(Billing_Account_Number__c),  BAN__c = null,   BAN__c = &apos;&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pre-Appointment Review</fullName>
        <actions>
            <name>Pre_Appointment_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In-Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsFielded__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>To remind the Case Rep to review the details of the case/appointment 1 day before the field service</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pre-Appointment Review Checkbox</fullName>
        <actions>
            <name>Pre_Appointment_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Pre_Appointment_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Creates a task when the Pre-Appointment Review checkbox is selected</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Survey Invite</fullName>
        <actions>
            <name>Survey_Invite_to_Customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Customer is invited to complete a survey after a case is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Completed Timestamp</fullName>
        <actions>
            <name>Auto_update_Completed_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Warranty,Closed</value>
        </criteriaItems>
        <description>Populates the Date/Time Completed field when the Case changes to warranty or closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Integration User1 Case Owner to Mobility - EN</fullName>
        <actions>
            <name>Update_Case_Owner_to_Mobility_EN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Integration User1</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Related Tasks%27 OwnerIDs</fullName>
        <actions>
            <name>Run_Update_Task_OwnerID_Flow</name>
            <type>FlowAction</type>
        </actions>
        <active>true</active>
        <description>Update Related Tasks&apos; OwnerIDs</description>
        <formula>CONTAINS( OwnerId , &quot;005&quot;)  &amp;&amp;  CONTAINS(PRIORVALUE( OwnerId) , &quot;00G&quot;) &amp;&amp;  X1st_time_trigger__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Warranty Review %28No Review Date%29</fullName>
        <actions>
            <name>Case_Warranty_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>To remind the Case Rep to review the case 48 hours after the case has been completed if no warranty review date is specified</description>
        <formula>AND(ISPICKVAL(Status,&quot;Warranty&quot;),ISNULL( Schedule_Close_Review__c )=TRUE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Warranty Review %28With Review Date%29</fullName>
        <actions>
            <name>Scheduled_Case_Warranty_Review</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>To remind the Case Rep to review the case on Scheduled Close Date</description>
        <formula>AND(ISPICKVAL(Status,&quot;Warranty&quot;),ISNULL( Schedule_Close_Review__c )=FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Case_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Due_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Review</subject>
    </tasks>
    <tasks>
        <fullName>Case_Warranty_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Due_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Warranty Review</subject>
    </tasks>
    <tasks>
        <fullName>Close_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Close_Review_Due_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Close Review</subject>
    </tasks>
    <tasks>
        <fullName>Email_Notification_to_Customer_Your_Case_has_been_addressed</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Notification to Customer &quot;Your Case has been addressed&quot;</subject>
    </tasks>
    <tasks>
        <fullName>Email_Notification_to_Customer_Your_Case_has_been_created</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Notification to Customer &quot;Your Case has been created&quot;</subject>
    </tasks>
    <tasks>
        <fullName>Email_Reminder_to_Customer_Field_Service_Appointment_in_2_days</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Reminder to Customer &quot;Field Service Appointment in 2 days&quot;</subject>
    </tasks>
    <tasks>
        <fullName>Order_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Order_Review_Due_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Order Review</subject>
    </tasks>
    <tasks>
        <fullName>Outcome_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Outcome_Review_Due_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Outcome Review</subject>
    </tasks>
    <tasks>
        <fullName>Pre_Appointment_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Pre_Appointment_Review_Due_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Pre-Appointment Review</subject>
    </tasks>
    <tasks>
        <fullName>Scheduled_Case_Warranty_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Schedule_Close_Review__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Scheduled Case Warranty Review</subject>
    </tasks>
    <tasks>
        <fullName>Survey_Invite_to_Customer</fullName>
        <assignedToType>creator</assignedToType>
        <description>Invite the customer to complete a survey after a case is closed</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.ClosedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Survey Invite to Customer</subject>
    </tasks>
</Workflow>
