@isTest
public class HelperTest { 
	
	@isTest
    public static void testIdPrefixes() {
        System.assertEquals(true, Helper.isContact('003q000000EjA2r'));
        System.assertEquals(true, Helper.isUser   ('005q0000001Do9T'));
        System.assertEquals(true, Helper.isQueue  ('00Gq0000000Hb45'));
    }
}