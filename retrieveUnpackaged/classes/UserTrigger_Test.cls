@isTest
private class UserTrigger_Test {
	
	@isTest static void test_User_Cant_Change_Sender_Name() {
		// ARRANGE
		UtlityTest util = new UtlityTest();
		User user = util.createTestUsr();

		// ACT
		user.SenderName = 'another name';
		update user;

		// ASSERT
		User user2 = [Select SenderName From User Where Id = :user.Id][0];
		System.assertEquals(null, user2.SenderName);
	}
}