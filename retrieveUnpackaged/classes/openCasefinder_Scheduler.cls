/*
	Author : Deepak Malkani.
	Created Date : March 13 2015
	Purpose : Scheduler class for open Case Finder and open Case Reminder Task Batch Job
*/
global class openCasefinder_Scheduler implements Schedulable {
	global void execute(SchedulableContext sc) {

		//run this only if nos of active jobs < 5 
		AsyncApexJob[] apexJobList = [SELECT id, JobType, Status FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
		if(apexJobList.Size() <= 5)
		{
			String Query = 'SELECT id, CaseNumber, OwnerId, Status FROM Case WHERE Status = \'Open\'';
			openCasefinder_Batch openCaseBatch = new openCasefinder_Batch(Query);
			database.executebatch(openCaseBatch);
		}
	}
}