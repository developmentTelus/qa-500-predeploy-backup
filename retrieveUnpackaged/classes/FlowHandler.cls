public class FlowHandler {
	// this method is not called from anywhere (not even this class)
	// it is only used to test the sub flow
	public static Map<String, Object> launch_RTS_Task_Fields(Case cse)
	{
		Flow.Interview.RTS_Task_Fields flow = new Flow.Interview.RTS_Task_Fields(
			new Map<String, Object>{
				'cse' => cse
			}
		);

        flow.start();

        return new Map<String, Object> {
		'taskSubject'		=> flow.getVariableValue('taskSubject'),
		'taskDescription'	=> flow.getVariableValue('taskDescription'),
		'taskDueDateDelta'	=> flow.getVariableValue('taskDueDateDelta')
        };
	}

	////////////////////////////
	/// HELPERS
	////////////////////////////


	private static DateTime toDateTime(Date d) {
		return DateTime.newInstance(d.year(), d.month(),d.day());
	}

	private static Date toDate(DateTime dt) {
		return Date.newInstance(dt.year(), dt.month(),dt.day());
	}
}