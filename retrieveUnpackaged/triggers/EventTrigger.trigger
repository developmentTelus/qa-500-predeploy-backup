/*
    Author : Deepak Malkani.
    Created Date : Feb 10 2015
    Purpose : The purpose of this trigger is to process all Trigger Logic for Events
*/

trigger EventTrigger on Event (before insert, after insert, before update, after update) {

    //We follow one COMMON Trigger Pattern here.
    //All Developers MUST ADHERE TO THIS Pattern. THERE MUST BE ONE TRIGGER PER OBJECT.

    //Initialise all Handlers and Collections here.
    EventTriggerHandler handler = new EventTriggerHandler();
    Set<ID> newOwnerIdSet = new Set<ID>();
    Set<ID> oldOwnerIdSet = new Set<ID>();

    if(trigger.isBefore){
        if(trigger.isInsert){

        }
        if(trigger.isUpdate && Outcome_StaticVars.canIRun() == true){

        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){

        }
        if(trigger.isUpdate && (test.isRunningTest() || Outcome_StaticVars.canIRun() == true)){
            newOwnerIdSet = handler.getNewOwnerIds(trigger.newMap);
            oldOwnerIdSet = handler.getOldOwnerIds(trigger.oldMap);
            handler.createEventHistEnteries(trigger.oldMap, trigger.newMap, newOwnerIdSet, oldOwnerIdSet);
            Outcome_StaticVars.stopTrigger();
        }
    }

    //clear off all collections here
    newOwnerIdSet.clear();
    oldOwnerIdSet.clear();
}