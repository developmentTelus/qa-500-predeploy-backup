/*
	Author : Deepak Malkani.
	Created Date : Feb 12 2015
	Purpose : The purpose of this test class is to Test the logic related to Task Event History Controller Extension
*/
@isTest(SeeAllData=false)
public class TaskEventHistoryCtrl_Ext_Test
{
	public static testMethod void UnitTest1()
	{
		//Initialise all Handler Classes
		UtlityTest testHandler = new UtlityTest();
		Task tkObj;
		List<Task_and_Event_History__c> tskEvtList = new List<Task_and_Event_History__c>();

		//Prepare Test Data first
		tkObj = testHandler.createTaskRec();
		Test.startTest();
		PageReference pg = Page.Task_History_Page;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(tkObj);
		//Instantiate the Controller Class
		TaskEventHistoryCtrl_Ext contHandler = new TaskEventHistoryCtrl_Ext(stdCtrl);
		tskEvtList = contHandler.getTaskHistory();
		system.assertEquals(0, tskEvtList.size());
		//Modify the Task Record now
		tkObj.Status = 'In Progress';
		update tkObj;
		tskEvtList.clear();
		tskEvtList = contHandler.getTaskHistory();
		system.assertEquals(1, tskEvtList.size());
		Test.StopTest();
	}
}