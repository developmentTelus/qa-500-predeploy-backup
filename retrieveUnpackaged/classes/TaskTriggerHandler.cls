/*
    Author : Deepak Malkani.
    Created Date : Feb 10 2015
    Purpose : Handler class to perform all Business Logic Processing related to Task
	
	Deepak Malkani : Adding this comment to test Bitbucket Versioning in a Maint Release 
	
*/

public with sharing class TaskTriggerHandler {
    //Default Constructor
    public TaskTriggerHandler() {}

    //Method used to collect New OwnerIds in a Set
    public Set<ID> getNewOwnerIds(Map<ID, Task> newMap)
    {
        Set<ID> newOwnerIds = new Set<ID>();
        for(Task tk : newMap.values())
            newOwnerIds.add(tk.OwnerId);
        return newOwnerIds;
    }

    //Method used to collect Own OwnerIds in a Set
    public Set<ID> getOldOwnerIds(Map<ID, Task> oldMap)
    {
        Set<ID> oldOwnerIds = new Set<ID>();
        for(Task tk : oldMap.values())
            oldOwnerIds.add(tk.OwnerId);
        return oldOwnerIds;
    }

	public void setTaskFields(List<Task> tknewList)
	{
		Set<ID> caseIds = new Set<ID>();
		Map<ID, Case> csMap = new Map<ID, Case>();
		
		//Get Case Records for the corresponding Task
		for(Task tknew : tknewList)
		{
			if(tknew.WhatId != null)
			{
				String caseId = tknew.WhatId;
				if(caseId.SubString(0,3) == '500')
				{
					caseIds.add(tknew.WhatId);
				}
			}
		}
		
		if(!caseIds.isEmpty())
			//get all contacts for these cases
			for(Case csObj : [SELECT id, ContactId, Can_Be_Reached__c FROM Case WHERE id IN : caseIds])
				csMap.put(csObj.Id, csObj);
		
		for(Task tkObj : tknewList)
		{
			if(!csMap.isEmpty())
			{
				if(csMap.get(tkObj.WhatId).ContactId != null && tkObj.WhoId == null)
				{
					system.debug('---> the contact is captured is '+csMap.get(tkObj.WhatId).ContactId);
					tkObj.WhoId = csMap.get(tkObj.WhatId).ContactId;
				}
				if(csMap.get(tkObj.WhatId).Can_Be_Reached__c != null && tkObj.Can_Be_Reached__c == null)
					tkObj.Can_Be_Reached__c = csMap.get(tkObj.WhatId).Can_Be_Reached__c;
			}
			if (tkObj.Type == 'Email' && tkObj.Status == 'Not Started')
			{
				tkObj.IsReminderSet = true;
				tkObj.Priority = 'High';
			}
		}
		
		
	}
	
    //Method used to create enteries in Task and Event History Object if Task field values are changed/updated.
    public void createTaskHistEnteries(Map<ID, Task> oldMap, Map<ID, Task> newMap, Set<ID> newOwnerSet, Set<ID> oldOwnerSet)
    {
        List<Task_and_Event_History__c> taskevtHistList = new List<Task_and_Event_History__c>();
        Map<ID, User> newUsrMap = new Map<ID, User>([SELECT id, Name FROM User WHERE id IN : newOwnerSet]);
        Map<ID, User> oldUsrMap = new Map<ID, User>([SELECT id, Name FROM User WHERE id IN : oldOwnerSet]);
        for(Task tkObj : newMap.values())
        {
            if((newMap.get(tkObj.Id).OwnerId != oldMap.get(tkObj.Id).OwnerId))
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = tkObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(tkObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Assigned To';
                tsevtHistObj.OldValue__c = oldUsrMap.get(oldMap.get(tkObj.Id).OwnerId).Id;
                tsevtHistObj.NewValue__c = newUsrMap.get(newMap.get(tkObj.Id).OwnerId).Id;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = tkObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Assignee from '+ oldUsrMap.get(oldMap.get(tkObj.Id).OwnerId).Name +' to ' +newUsrMap.get(newMap.get(tkObj.Id).OwnerId).Name;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(tkObj.Id).Status != oldMap.get(tkObj.Id).Status))
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = tkObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(tkObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Status';
                tsevtHistObj.OldValue__c = oldMap.get(tkObj.Id).Status;
                tsevtHistObj.NewValue__c = newMap.get(tkObj.Id).Status;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = tkObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Status from '+ oldMap.get(tkObj.Id).Status +' to ' +newMap.get(tkObj.Id).Status;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(tkObj.Id).Activity_Date__c != oldMap.get(tkObj.Id).Activity_Date__c))
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = tkObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(tkObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Activity_Date__c';
                tsevtHistObj.OldValue__c = oldMap.get(tkObj.Id).Activity_Date__c;
                tsevtHistObj.NewValue__c = newMap.get(tkObj.Id).Activity_Date__c;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = tkObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Activity Date from '+ oldMap.get(tkObj.Id).Activity_Date__c +' to ' + newMap.get(tkObj.Id).Activity_Date__c;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(tkObj.Id).ActivityDate != oldMap.get(tkObj.Id).ActivityDate))
            {
                String oldDate;
                String newDate;
                if(newMap.get(tkObj.Id).ActivityDate != null)
                    newDate = newMap.get(tkObj.Id).ActivityDate.format();
                else
                    newDate = null;
                if(oldMap.get(tkObj.Id).ActivityDate != null)
                    oldDate = oldMap.get(tkObj.Id).ActivityDate.format();
                else
                    oldDate = null;
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = tkObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(tkObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Due Date';
                tsevtHistObj.OldValue__c = null;
                tsevtHistObj.NewValue__c = null;
                if(oldDate != null)
                    tsevtHistObj.OldValueDateTime__c = Datetime.newInstanceGmt(oldMap.get(tkObj.Id).ActivityDate.year(), oldMap.get(tkObj.Id).ActivityDate.month(), oldMap.get(tkObj.Id).ActivityDate.day(), system.now().hourGmt(), system.now().minuteGmt(), system.now().secondGmt());
                else
                    tsevtHistObj.OldValueDateTime__c = null;
                if(newDate != null)
                    tsevtHistObj.NewValueDateTime__c = Datetime.newInstanceGmt(newMap.get(tkObj.Id).ActivityDate.year(), newMap.get(tkObj.Id).ActivityDate.month(), newMap.get(tkObj.Id).ActivityDate.day(), system.now().hourGmt(), system.now().minuteGmt(), system.now().secondGmt());
                else
                    tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = tkObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Due Date from '+ oldDate +' to ' +newDate;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(tkObj.Id).Description != oldMap.get(tkObj.Id).Description))
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = tkObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(tkObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Comments';
                tsevtHistObj.OldValue__c = oldMap.get(tkObj.Id).Description;
                tsevtHistObj.NewValue__c = newMap.get(tkObj.Id).Description;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = tkObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Comments';
                system.debug('----> i came here to update new values of comments');
                taskevtHistList.add(tsevtHistObj);
            }
        }
        if(!taskevtHistList.isEmpty())
            insert taskevtHistList;

        //clear off all collections
        taskevtHistList.clear();
        oldMap.clear();
        newMap.clear();
        newOwnerSet.clear();
        oldOwnerSet.clear();            
    }
}