/*
###########################################################################
# File..................: ContactTriggerHandler.cls
# Version...............: 1.0
# Created by............: TechM 
# Created Date..........: 17-Apr-2015
# Description...........: Controller handler class for Contact Trigger

# Copyright (c) Tech Mahindra. All Rights Reserved.
#
# Created by the Tech Mahindra. Modification must retain the above copyright #notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Bell Mobility, is hereby forbidden. Any modification to #source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
*/

public with sharing class ContactTriggerHandler {
	/**
	 *	Method 	: doNotAllowDML
	 *	Param	: List<Contact>
	 *	Return	: void
	 *	Description	: method to disallow update DML for the contact records which are not created by the IntegrationUser
	 *					i.e. SourceRefID__c != null then disallow update
	 */
	 
	public static void doNotAllowDML(List<Contact> lstNewContacts){
		
		//Iterate over each of the record and chekc if sourceRefId__c is null
		for(Contact curCnt : lstNewContacts){
			string sri = curCnt.SourceRefID__c;
			if(sri != null && sri.trim().equals('') == false){
				//SourceRefId is present
				curCnt.addError(Label.Contacts_Cannot_Be_Edited);
			}
		}
		
	}
	
}