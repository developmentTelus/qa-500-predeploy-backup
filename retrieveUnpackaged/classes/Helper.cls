/*
    Author : Haydar Hadi
    Purpose : APEX Handler class used for general purpose operations.
*/

public class Helper { 

    public static Boolean isQueue(String id) {
        return id.substring(0,3) == '00G';
    }

    public static Boolean isUser(String id) {
        return id.substring(0,3) == '005';
    }

    public static Boolean isContact(String id) { 
        return id.substring(0,3) == '003';
    }


    ///////////////////////////////////////////

    public final static String SOURCETYPE_RPT1_RENEWAL = 'RPT1-RENEWAL';
    public final static String SOURCETYPE_RPT1_ACTIVATION = 'RPT1-ACTIVATION';
    public final static String SOURCETYPE_RTS_REPAIR = 'RTS-REPAIR';
    public final static String SOURCETYPE_LNR_RENEWAL = 'LNR-RENEWAL';
    public final static String SOURCETYPE_OMS = 'OMS-ORDERS';
    public final static String SOURCETYPE_Concierge = 'OMS-CONCIERGE';
    public final static String SOURCETYPE_GCM = 'OMS-GCM';

    public static Boolean isRTS(Case cse) {
        return isCaseForSystem(cse, SOURCETYPE_RTS_REPAIR);
    }   

    public static Boolean isOMS(Case cse) {
        return isCaseForSystem(cse, SOURCETYPE_OMS);
    }   
    
    public static Boolean isConcierge(Case cse) {
        return isCaseForSystem(cse, SOURCETYPE_Concierge);
    } 
    
    public static Boolean isGCM(Case cse) {
        return isCaseForSystem(cse, SOURCETYPE_GCM);
    } 

    public static Boolean isRPT1Renewal(Case cse) {
        return isCaseForSystem(cse, SOURCETYPE_RPT1_RENEWAL); 
    }

    public static Boolean isProactiveRenewal(Case cse) {
        return isCaseForSystem(cse, SOURCETYPE_LNR_RENEWAL); 
    }

    public static Boolean isCaseForSystem(Case cse, String sourceType) {
        return (cse!=null && cse.Source_Type__c !=null && cse.Source_Type__c == sourceType);
    }   

    /*
    public final static String QUEUE_FFH_ONBOARDING_EN = 'FFH_Onboarding_EN';
    public final static String QUEUE_MOB_RENEWALS_PROACTIVE_EN = 'MOB_Renewals_Proactive_EN';

    public static Boolean is_FFH_Onboarding_EN(Case cse) {
        return isCaseForSystem(cse, QUEUE_FFH_ONBOARDING_EN);
    }   
    */


}