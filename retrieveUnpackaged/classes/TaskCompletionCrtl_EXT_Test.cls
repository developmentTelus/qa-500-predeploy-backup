/*
  Author : Praveen Bonalu.
  Created Date : Mar 12 2015
  Purpose : The purpose of this test class is to Test the logic related to TaskCompletionCrtl_EXT   Controller Extension
*/

@isTest 
public class TaskCompletionCrtl_EXT_Test {
    @isTest static void unitTest1()
    {
        Case c=new case(Status='open ');  
        insert c;
        
        task tk =new task();
        tk.Status='Not Started';
        tk.Priority='High';
        tk.WhatId =c.id;
        tk.subject='Pre-Appointment Review';
        tk.description='task closing comments';
        tk.ActivityDate = system.today();
        insert tk;
        
        test.setCurrentPage(page.TaskCompletePage);
        ApexPages.CurrentPage().getParameters().put('id',tk.Id);
        Apexpages.StandardController con = new ApexPages.StandardController(tk);
        TaskCompletionCrtl_EXT tcxt= new TaskCompletionCrtl_EXT(con);
        tk.status='Completed';
        tcxt.Save();
        tcxt.redirectPage();
       
        system.assertEquals('Completed', [SELECT Status FROM TASK where id=:tk.Id LIMIT 1].Status);
        
    }
}