/*
	Author : Deepak Malkani.
	Created Date : Feb 12 2015
	Purpose : This is the test class for Event History Controller Ext Class
*/
@isTest(SeeAllData=false)
public class EventHistoryCtrl_Ext_Test
{
	public static testMethod void UnitTest1()
	{
		//Initialise all Handler Classes
		UtlityTest testHandler = new UtlityTest();
		Event evtObj;
		List<Task_and_Event_History__c> tskEvtList = new List<Task_and_Event_History__c>();

		//Prepare Test Data first
		evtObj = testHandler.createEventRec();
		Test.startTest();
		PageReference pg = Page.Event_History_Page;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(evtObj);
		//Instantiate the Controller Class
		EventHistoryCtrl_Ext contHandler = new EventHistoryCtrl_Ext(stdCtrl);
		tskEvtList = contHandler.getEventHistory();
		system.assertEquals(0, tskEvtList.size());
		//Modify the Task Record now
		evtObj.Type = 'Email';
		update evtObj;
		tskEvtList.clear();
		tskEvtList = contHandler.getEventHistory();
		system.assertEquals(3, tskEvtList.size());
		Test.StopTest();		
	}
}