/*
    Author : Deepak Malkani.
    Created Date : 
    Modified by:Praveen Bonalu.Changed the Finish Method(Removed the Email Address of the Inactive User)
    Modified Date:24-08-2015
*/

global class openCaseReminderTask_Batch implements Database.Batchable<sObject>{
    
    public String query;
    
    global openCaseReminderTask_Batch(String q) {       
        query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Batch_Logger__c> scope) {
    
        Set<ID> caseIds = new Set<ID>();
        String Owner;
        Map<ID, ID> taskCaseMap = new Map<ID, ID>();
        //Map<ID, Task> tkMap = new Map<ID,Task>();
        List<Task> tkupsertList = new List<Task>();
        List<Batch_Logger__c> batchLogList = new List<Batch_Logger__c>();
        String error;
        
        //collect all the case ids in a set
        for(Batch_Logger__c bObj : scope)
            caseIds.add(bObj.Case__c);
        
        //Query on all the cases which are picked up in Todays Batch run
        for(Case csObj : [SELECT id, CaseNumber, OwnerId, (SELECT id, Subject, Status,IsReminderSet,ReminderDateTime FROM Tasks WHERE isClosed = false)
                            FROM Case
                            WHERE id IN : caseIds AND Status = 'Open'])
        {
            Owner = csObj.OwnerId;
            //We dont want to work on cases, which come via ETL runs.
            if(csObj.Tasks.Size() == 0 && Owner.substring(0,3) == '005'){
                Task tkObj = new Task();
                tkObj.WhatId = csObj.Id;
                tkObj.Subject = 'Close Case Reminder';
                tkObj.Description = Label.Close_Case_Reminder_Comments1+' '+ system.now() + ' . '+Label.Close_Case_Reminder_Comments2;
                tkObj.Priority = 'Low';
                tkObj.ActivityDate = system.today();
                tkObj.OwnerId = csObj.OwnerId;
                tkObj.IsReminderSet = true;
                tkObj.ReminderDateTime = system.now();
                tkupsertList.add(tkObj);
                taskCaseMap.put(tkObj.Id, csObj.Id);
            }
            else if(csObj.Tasks.Size() == 1 && Owner.substring(0,3) == '005' && csObj.Tasks[0].IsReminderSet == false)
            {
                Task tkObj = new Task(Id = csObj.Tasks[0].Id);
                tkObj.IsReminderSet = true;
                tkObj.ReminderDateTime = system.now();
                tkupsertList.add(tkObj);
                taskCaseMap.put(tkObj.Id, csObj.Id);
            }
        }
        
        if(!tkupsertList.isEmpty())
        {
            List<Database.UpsertResult> upsList = database.upsert(tkupsertList, false);
            for(Database.UpsertResult result : upsList)
            {
                if(result.isSuccess())
                    error = null;
                else
                    for(Database.Error err : result.getErrors())
                        error = 'Error Status Code : '+err.getStatusCode()+'. Error Message : '+err.getMessage()+'. and Error on Fields : '+err.getFields();
            }
        }
        
        
        if(error == null)
        {
            //The table also needs to be updated with errors, in case there are any
            for(Batch_Logger__c bObj : [SELECT id, Batch_Status__c, Error_Details__c
                                        FROM Batch_Logger__c WHERE id IN : scope])
            {
                Batch_Logger__c bObjUpd = new Batch_Logger__c(Id = bObj.Id);
                bObjUpd.Batch_Status__c = 'Completed';
                bObjUpd.Error_Details__c = null;
                batchLogList.add(bObjUpd);
            }
        }
        else
        {
            //The table also needs to be updated with errors, in case there are any
            for(Batch_Logger__c bObj : [SELECT id, Batch_Status__c, Error_Details__c
                                        FROM Batch_Logger__c WHERE id IN : scope])
            {
                Batch_Logger__c bObjUpd = new Batch_Logger__c(Id = bObj.Id);
                bObjUpd.Batch_Status__c = 'Error';
                bObjUpd.Error_Details__c = error;
                batchLogList.add(bObjUpd);
            }
        }
        
        if(!batchLogList.isEmpty())
            update batchLogList;
    }
    
    global void finish(Database.BatchableContext BC) {
    
        DateTime todaysDate = system.now();
        List<Integer> recordTracker = new List<Integer>();
        if(!Test.isRunningTest())
        {
            List<AggregateResult> aggResultSum = [SELECT count(Id) FROM Batch_Logger__c WHERE SystemModstamp <= : system.now() AND SystemModstamp >= : system.now().addDays(-1)];
            List<AggregateResult> aggResultFail = [SELECT count(Batch_Status__c) FROM Batch_Logger__c WHERE SystemModstamp  <= : system.now() AND SystemModstamp >= : system.now().addDays(-1) AND Batch_Status__c = 'Error'];
        
            // Send an email to the Apex job's submitter notifying of job completion.
            if(!aggResultSum.isEmpty())
                for(AggregateResult aResult : aggResultSum)
                    recordTracker.add((Integer)aResult.get('expr0'));   
            if(!aggResultFail.isEmpty())
                for(AggregateResult aResult : aggResultFail)
                    recordTracker.add((Integer)aResult.get('expr0'));
        }
        //system.debug('----> The aggregate result Sum is '+aggResultSum + 'aggregate result failure is '+aggResultFail);
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'greg.lebel@telus.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Open Case Reminders ' + a.Status);
        if(test.isRunningTest())
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
        if(!recordTracker.isEmpty() && (!test.isRunningTest()))
        {
            Report rpt = [SELECT id FROM Report WHERE Name = 'Open Cases with no Activities' LIMIT 1];
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +' batches today. Total number of records were : '+ recordTracker[0] + '. Total number of failures were : ' + recordTracker[1] + ' The Report Link can be found here : '+Label.Salesforce_URL+'/'+rpt.Id);
            mail.setHtmlBody('The batch Apex job processed ' + a.TotalJobItems +' batches today. Total number of records were : '+ recordTracker[0] + '. Total number of failures were : ' + recordTracker[1] + ' The Report Link can be found here : '+Label.Salesforce_URL+'/'+rpt.Id);
        }
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    

}