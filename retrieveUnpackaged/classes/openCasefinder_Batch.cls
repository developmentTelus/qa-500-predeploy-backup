/*
    Author : Deepak Malkani.
    Created Date : March 10 2015
    Purpose : Batch job which looks out for Open cases which has all Activities completed, and sets a remider task for these cases.
*/

global class openCasefinder_Batch implements Database.Batchable<sObject> {
    
    public String query;
    
    global openCasefinder_Batch(String q) {
        query = q;      
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
    
        List<Batch_Logger__c> bLoggerList = new List<Batch_Logger__c>();
        String Owner;
        
        //Query on all cases which are still open and no longer have any open tasks
        for(Case csObj : [SELECT id, CaseNumber, OwnerId, (SELECT id, Subject, Status,IsReminderSet,ReminderDateTime FROM Tasks WHERE isClosed = false)
                            FROM Case
                            WHERE id IN : scope AND Status = 'Open' AND Source_Type__c != 'RTS-REPAIR'])
        {
            Owner = csObj.OwnerId;
            //We dont want to work on cases, which come via ETL runs.
            if(csObj.Tasks.Size() == 0 && Owner.substring(0,3) == '005')
            {
                //Create a Batch Log enteries
                Batch_Logger__c bLog = new Batch_Logger__c();
                bLog.Batch_Process_Name__c = 'Reminder Tasks for Open Case';
                bLog.Batch_Status__c = 'Not Started';
                bLog.Case__c = csObj.Id;
                bLog.Error_Details__c = null;
                bLoggerList.add(bLog);
            }
            else if (csObj.Tasks.Size() == 1  && csObj.Tasks[0].Subject == 'Close Case Reminder' && csObj.Tasks[0].IsReminderSet == false)
            {
                Batch_Logger__c bLog = new Batch_Logger__c();
                bLog.Batch_Process_Name__c = 'Reminder Tasks for Open Case';
                bLog.Batch_Status__c = 'Not Started';
                bLog.Case__c = csObj.Id;
                bLog.Error_Details__c = null;
                bLoggerList.add(bLog);
            }
        }
        
        if(!bLoggerList.isEmpty())
            insert bLoggerList;
    }
    
    global void finish(Database.BatchableContext BC) {
        
        //Call the next nested Batch
        String query = 'SELECT id, Case__c, Batch_Status__c, Error_Details__c, Batch_Process_Name__c FROM Batch_Logger__c WHERE Batch_Status__c = \'Not Started\' OR Batch_Status__c = \'Error\'';
        database.executeBatch(new openCaseReminderTask_Batch(query));
    }   
}