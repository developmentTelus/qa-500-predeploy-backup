/*
	Author : Deepak Malkani.
	Created Date : Feb 28 2015
	Purpose : Unit Test class for Case Trigger Handler class
*/
@isTest(SeeAllData=false)
public class CaseTriggerHandler_Test
{
	public static UtlityTest testUtil = new UtlityTest();

	/*
		Haydar Hadi : Test Method used to check all tasks created for a case record. Since the case
		does not have Order Review and Pre-Appintment flags checked, cancelled task would be created.
		name: test_new_case_has_4_tasks_2_Not_Started_2_Cancelled
	*/
	@isTest
	public static void testScenario01()
	{
		// ARRANGE

		// ACT
		Contact con = testUtil.createContactRec();
		Case csObj = testUtil.createCaseRec(con);

		// ASSERT
		List<Task> allTasks = testUtil.getAllTasks(csObj);

		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Not Started', 'Courtesy Call').size());
		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Not Started', 'Bill Review').size());
		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Cancelled', 'Order Review').size());
		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Cancelled', 'Pre-Appointment Review').size());
	}

	/*
		Haydar Hadi : Test Method creates an Order Review Case with 3 open and 2 cancelled tasks
		name: test_new_case_wt_order_review_has_3_tasks_2_Not_Started_1_Cancelled
	*/
	@isTest
	public static void testScneario02()
	{
		// ARRANGE

		// ACT
		Contact con = testUtil.createContactRec();
		Case csObj = testUtil.createCaseRecwtOrderReview(con);

		//ASSERT
		List<Task> allTasks = testUtil.getAllTasks(csObj);

		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Not Started', 'Courtesy Call').size());
		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Not Started', 'Bill Review').size());
		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Not Started', 'Order Review').size());
		system.assertEquals(1, testUtil.filterStatusSubject(allTasks, 'Cancelled', 'Pre-Appointment Review').size());
	}

	/*
		Haydar Hadi : Test Method to test creation of cases, with different user scenario.
		name: test_creating_case_as_different_user_then_assign_to_group
	*/
	@isTest
	public static void testScenario04()
	{
		// ARRANGE
		Contact con = testUtil.createContactRec();
		Case csObj = testUtil.createCasewithGrp(con);

		User u = testUtil.createTestUsr();
		System.runAs(u)
		{
			Case[] csObjt = [SELECT id, OwnerId FROM Case LIMIT 1];
			System.assertEquals(1, csObjt.size(), 'there should be one created case');


			// ACT
			Outcome_StaticVars.canRun = true;
			Case c = new Case(Id = csObjt[0].Id);
			c.OwnerId = u.Id;
			update c;

			// ASSERT - Technical
			System.assertEquals(u.Id, c.OwnerId, 'the onwer of the case is set correctly.');
			System.assertEquals(false, Outcome_StaticVars.canIRun(), 'Static variable should be false after ');

			// ASSERT - Business
			system.assertEquals(3, testUtil.getAllTasks(c, 'Not Started').size());
		}
	}

	/*
		Haydar Hadi : RTS Test case to check for Non Apple Cases and their tasks - JIRA # CES20-311
		name: test01_updating_rts_nonapple_status_to_submitted_will_create_courtesy_task
	*/
	@isTest
	public static void testScenario05() {

		// ARRANGE
		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'RTS-REPAIR', Source_Action_Cd__c = 'Repair and Return');
		insert cse;

		// ACT
		Outcome_StaticVars.canRun = true;
		cse.SourceStatus__c = 'Submitted';
		update cse;

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(1, tasks.size());
		System.assertEquals('Courtesy Call', tasks[0].Subject);
		System.assert(tasks[0].Description.StartsWith('MUST CALL CUSTOMER - Repair'));
	}

	/*
		Haydar Hadi : Test Method used to create a RTS Case of Action Type - Apple Delayed Repair.
		This case will create a Close Review Task when agent does a get Case - JIRA # CES20-311.
		name: test02_updating_rts_case_applydelayedrepair_status_to_submitted_will_create_closereview_task
	*/
	@isTest
	public static void testScenario06() {

		// ARRANGE
		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'RTS-REPAIR', Source_Action_Cd__c = 'APPLE OTC Delay');
		insert cse;

		// ACT
		Outcome_StaticVars.canRun = true;
		cse.SourceStatus__c = 'Submitted';
		update cse;

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(1, tasks.size());
		System.assertEquals('Close Review', tasks[0].Subject);
		System.assert(tasks[0].Description.StartsWith('IMPRESS: Please contact'));
	}

	/*
		Haydar Hadi : Test Method used to replicate Get Case functionlaity for an RTS Specific Case. JIRA # CES20-311
		name: test03_moving_rts_case_from_queue_to_user_creates_task
	*/
	@isTest
	public static void testScenario07() {

		// http://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
		System.runAs ( new User(Id = UserInfo.getUserId()) ) {

			// ARRANGE
			Group queue = new Group(Name='test queue', type='Queue');
			insert queue;
			QueuesObject qo = new QueueSObject(QueueID = queue.id, SobjectType = 'Case');
			insert qo;
			Contact contact = new Contact(LastName = 'Test');
			insert contact;
			Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'RTS-REPAIR', SourceStatus__c = 'Submitted', Source_Action_Cd__c='Repair and Return',  OwnerId = queue.Id);
			insert cse;

			// ACT
			Outcome_StaticVars.canRun = true;
			cse.OwnerId = UserInfo.getUserId();
			update cse;

			// ASSERT
			List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
			System.assertEquals(1, tasks.size());
			System.assertEquals('Courtesy Call', tasks[0].Subject);
			System.assert(tasks[0].Description.StartsWith('MUST CALL CUSTOMER - Repair'));
		}
	}

	/*
		Haydar Hadi : Test method to update an existing RTS Case. No tasks will be created for this case,
		unless and until owner is a valid User/Agent. JIRA # CES20-311
		name: test04_updating_rts_case_in_in_queue_wont_create_task
	*/
	@isTest
	public static void testScenario08() {

		// http://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
		System.runAs ( new User(Id = UserInfo.getUserId()) ) {

			// ARRANGE
			Group queue = new Group(Name='test queue', type='Queue');
			insert queue;
			QueuesObject qo = new QueueSObject(QueueID = queue.id, SobjectType = 'Case');
			insert qo;

			Contact contact = new Contact(LastName = 'Test');
			insert contact;
			Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'RTS-REPAIR', SourceStatus__c = 'Submitted', OwnerId = queue.Id);
			insert cse;

			// ACT
			Outcome_StaticVars.canRun = true;
			cse.SourceStatus__c = 'Ready for pickup';
			update cse;

			// ASSERT
			List<Task> tasks = [Select Id, Description, Subject From Task];
			System.assertEquals(0, tasks.size());
		}
	}

	/*
		Haydar Hadi : Test Method used to update a case with Integration User1 as owner. Only the case gets
		updated, no tasks will be created whatsoever.
		name: test05_updating_case_by_integration_user_will_NOT_change_ownership
	*/
	@isTest
	public static void testScenario09() {

		// ARRANGE
		UtlityTest util = new UtlityTest();
		User integrationUser = util.createTestUsr('Integration Profile', 'Integration', 'User1');
		ETLLoads__c customSetting = new ETLLoads__c(SetupOwnerId = integrationUser.Id, runDataLoads__c = true);
		insert customSetting;

		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, OwnerId = UserInfo.getUserId(), SourceStatus__c = 'Submitted');
		insert cse;


		// ACT
		Outcome_StaticVars.canRun = true;

		System.runAs(integrationUser) {
			cse.SourceStatus__c = 'Ready for pickup';
			cse.OwnerId = integrationUser.Id;
			update cse;
		}

		// ASSERT
		Case cse2 = [Select Id, OwnerId From Case where Id =: cse.Id];
		System.assertEquals(UserInfo.getUserId(), cse2.OwnerId);
	}

	@isTest
	public static void testScenario10() {
	    
	    // ARRANGE

	    QueueSObject q;
	    Group g;
		System.runAs([Select Id From User where Id = :UserInfo.getUserId()][0]) {
			g = new Group(Type='Queue', Name='MOB_Renewals_Proactive_EN"');
			insert g;
			q = new QueueSObject(SobjectType='Case', QueueId=g.Id);
			insert q;       
		}
		
		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'LNR-RENEWAL', OwnerId = g.Id);
		insert cse;

		// ACT

	    Outcome_StaticVars.canRun = true;

		cse.OwnerId = UserInfo.getUserId();
		update cse;

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(1, tasks.size());
		System.assertEquals('Renewal Call', tasks[0].Subject);

		List<Task> tasks2 = [Select Id, Description, Subject From Task where     Subject like 'Email%'];
		System.assertEquals(0, tasks2.size());
	}


	@isTest
	public static void testScenario11() {
	    
	    // ARRANGE

		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'LNR-RENEWAL');
		insert cse;

		// ACT

	    Outcome_StaticVars.canRun = true;

		cse.Description = 'test';
		update cse;

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(0, tasks.size());
	}


	@isTest
	public static void testScenario12() {
	    
	    // ARRANGE

	    QueueSObject q;
	    Group g;
		System.runAs([Select Id From User where Id = :UserInfo.getUserId()][0]) {
			g = new Group(Type='Queue', Name='MOB_Renewals_Proactive_EN"');
			insert g;
			q = new QueueSObject(SobjectType='Case', QueueId=g.Id);
			insert q;       
		}
		
		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'LNR-RENEWAL', OwnerId = g.Id);
		insert cse;

		// ACT

	    Outcome_StaticVars.canRun = true;

		cse.Description = 'test';
		update cse;

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(0, tasks.size());
	}	

	// OMS
	@isTest
	public static void testScenario13() {
	    
	    // ARRANGE

	    QueueSObject q;
	    Group g;
		System.runAs([Select Id From User where Id = :UserInfo.getUserId()][0]) {
			g = new Group(Type='Queue', Name='FFH_Onboarding_Reactive_EN"');
			insert g;
			q = new QueueSObject(SobjectType='Case', QueueId=g.Id);
			insert q;       
		}
		
		Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Date today = Date.today();
		Date nextMonth = today.addMonths(1);
		Case cse = new Case(ContactId = contact.Id, Source_Type__c = 'OMS-ORDERS', OwnerId = g.Id, Due_Date__c=today, Next_Billing_Date1__c = nextMonth);
		insert cse;

		// ACT

	    Outcome_StaticVars.canRun = true;

		cse.OwnerId = UserInfo.getUserId();
		update cse;

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%' order by Subject];
		System.assertEquals(4, tasks.size());
		
		System.assertEquals('Bill Review', tasks[0].Subject);
		System.assertEquals('Courtesy Call', tasks[1].Subject);
		System.assertEquals('Order Review', tasks[2].Subject);
		System.assertEquals('Pre-Appointment Review', tasks[3].Subject);

		List<Task> tasks2 = [Select Id, Description, Subject From Task where     Subject like 'Email%'];
		System.assertEquals(0, tasks2.size());
	}	



	/*
		Deepak Malkani : This Unit Test Method is used to create a single Case Record.
		Since CS is not turned on, we expect it to create tasks for a newly created case.
	*/
	@isTest
	public static void UnitTest1()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		Test.startTest();
		Contact con = testHandler.createContactRec();
		Case csObj = testHandler.createCaseRec(con);
		Test.stopTest();
		//make assertions
		List<Task> tkList = [SELECT id FROM Task WHERE WhatId =: csObj.Id and Status = 'Not Started'];
		system.assertEquals(2, tkList.size());
	}

	/*
		Deepak Malkani : This Unit Test method creates a single Case record for a Contact
		Since CS is not turned on, on creation of a new case, tasks would be created for the newly created case.
	*/
	@isTest
	public static void UnitTest2()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		Test.startTest();
		Contact con = testHandler.createContactRec();
		Case csObj = testHandler.createCaseRecwtOrderReview(con);
		Test.stopTest();
		//make assertions
		List<Task> tkList = [SELECT id FROM Task WHERE WhatId =: csObj.Id and Status = 'Not Started'];
		system.assertEquals(3, tkList.size());
	}

	/*
		Deepak Malkani : This Test Method creates a new case assigned to Mobility Group.
		After which the case is updated again- ownerid performing get Case functionlaity
	*/
	@isTest
	public static void UnitTest3()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		User u = testHandler.createTestUsr();
		Test.startTest();
		system.runAs(new User (Id = UserInfo.getUserId()))
		{
			Contact con = testHandler.createContactRec();
			Case csObj = testHandler.createCasewithGrp(con);
		}
		System.runAs(u)
		{
			Case[] csObjt = [SELECT id, OwnerId FROM Case LIMIT 1];
			system.debug('--> case object list is '+csObjt);
			//Outcome_StaticVars.stopTrigger();
			Outcome_StaticVars.canRun = true;
			Case c = new Case(Id = csObjt[0].Id);
			c.OwnerId = u.Id;
			update c;
			//make assertions
			List<Task> tkList = [SELECT id FROM Task Where WhatId =: c.Id anD Status = 'Not Started' and (not Subject like 'Email%')];
			system.assertEquals(3, tkList.size());
		} 
		Test.stopTest();
	}

	/*
		Deepak Malkani : This method tests a fake scenario, here a case is created with a valid owner and not Group
		After this case owner is changed, performing a get case, but get case logic should not fire as previous owner is not
		a Group.
	*/
	@isTest
	public static void UnitTest4()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		Test.startTest();
		Account accnt = testUtil.createAccntRec();
		Case csObj = testHandler.createCasewtAcnt(accnt);
		//reload the static variable again, for the update to work again.
		Outcome_StaticVars.canRun = true;
		csObj.OwnerId = UserInfo.getUserId();
		update csObj;
		Test.stopTest();
		//make assertions
		List<Task> tkList = [SELECT id, Subject, Description FROM Task WHERE WhatId =: csObj.Id];

		for(Task t: tkList) {
			System.debug('---HDR---');
			System.debug('Id:' + t.Id);
			System.debug('Description:' + t.Description);
			System.debug('Subject:' + t.Subject);
		}

		system.assertEquals(4, tkList.size()); 	}


	/*	
		Deepak Malkani : Test created to perform some Bulk Testing. Here 5 new cases are created
		Based on the logic, 15 new tasks will be created based on these 5 cases.
	*/
	@isTest
	public static void UnitTest5()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		Test.startTest();
		Contact con = testHandler.createContactRec();
		List<Case> csList = testHandler.createMultiCaseRec(5, con);
		insert csList;
		Test.stopTest();
		//make assertions
		List<Task> tkList = [SELECT id FROM Task WHERE WhatId IN : csList and Status = 'Not Started' and (not Subject like 'Email%')];
		system.assertEquals(15, tkList.size());
	}
	/*
		Deepak Malkani : Test used to replicate Integration User Scenario-loading cases.
		Since CS is turned on, cases will be created, but no tasks.
	*/
	@isTest
	public static void UnitTest6()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		testHandler.setCustomSettings();
		Test.startTest();
		Contact con = testHandler.createContactRec();
		List<Case> csList = testHandler.createMultiCaseRec(1, con);
		insert csList;
		Test.stopTest();
		//make assertions
		List<Task> tkList = [SELECT id FROM Task WHERE WhatId IN : csList and Status = 'Not Started' and (not Subject like 'Email%')];
		system.assertEquals(0, tkList.size());
	}

	/*
		Deepak Malkani : Test method used to test the Close Case reminder Functionality. Here cases with no tasks
		are created first. Then close case reminder task is added to the case. If the case is closed now, reminder tasks
		will automatically close.
	*/
	@isTest
	public static void UnitTest7()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		List<Case> csList = new List<Case>();
		List<Task> tkList = new List<Task>();
		List<Case> csUpdList = new List<Case>();

		csList = testHandler.createopenCaseswtNoTasks(2);
		for(Case csObj : csList)
			tkList.add(testHandler.createClsCaseRemindTaskRec(csObj.Id));
		if(!tkList.isEmpty())
			insert tkList;
		//Closing all the Cases now from Open to Pending Close
		for(Case cs : csList)
		{
			Case caseObj = new Case(Id = cs.Id);
			caseObj.Status = 'Pending Close';
			csUpdList.add(caseObj);
		}
		if(!csUpdList.isEmpty())
			update csUpdList;
		csList.clear();
		tkList.clear();
		csList = [SELECT id, Status FROM Case WHERE Status = 'Pending Close'];
		//making assertions
		system.assertEquals(2, csList.size());
	}

	/*
		Deepak Malkani : Test Method to create a RPT1-Renewal Case. Here case is created by Integration User
		and then agent does a get Case which creates RPT1-Renewal tasks.
	*/
	@isTest
	public static void UnitTest10()
	{
		//Creating Test Data
		UtlityTest testHandler = new UtlityTest();
		User u = [SELECT id FROM User WHERE Name = 'Integration User1' LIMIT 1];
		//Start Testing
		Test.startTest();
		ETLLoads__c CS = testHandler.setCustomSettings(); //CS Set to true to bypass all trigger logic
		Contact con = testHandler.createContactRecWithSourceRef();
		Case c = testHandler.createCaseRec(con, u);
		CS.runDataLoads__c = false;
		update CS; //uncheck the CS, so that trigger gets fired now if OwnerId is changed.
		//Outcome_StaticVars.canRun = true;
		c.Source_Type__c = 'RPT1-Renewal';
		c.OwnerId = UserInfo.getUserId();
		update c;
		List<Task> tkList = [SELECT id, WhatId FROM Task WHERE WhatId =: c.Id];
//		system.assertEquals(2, tkList.Size());
	}
}