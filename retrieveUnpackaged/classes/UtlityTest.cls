/*
###########################################################################
# File..................: UtlityTest.cls (typo in the file name)
# Created by............: TechM - Deepak Malkani
# Created Date..........: 15-11-2015
# Description...........: A common Utility class/handler which helps creating test data for all test classes in the SFDC Org.
#
# Copyright (c) Tech Mahindra. All Rights Reserved.
#
# Created by the Tech Mahindra. Modification must retain the above copyright #notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Bell Mobility, is hereby forbidden. Any modification to #source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# *********** VERSION UPDATES ***********************************************************************
#
# 15-11-2015    Deepak Malkani        Created File
#
# ***************************************************************************************************
# Internal Coding Guidelines: https://project.collaborate.tsl.telus.com/sites/outcome/SitePages/Coding%20Guidelines.aspx
# ***************************************************************************************************
*/

@isTest(SeeAllData=false)
public with sharing class UtlityTest {

	public UtlityTest() {
	}

	// create a single Task Record
	public Task createTaskRec()
	{
		Task tkObj = new Task(
			Subject = 'Courtesy Call',
			OwnerId = UserInfo.getUserId(),
			Status = 'Not Started',
			ActivityDate = system.today(),
			Priority = 'Medium',
			Activity_Date__c = String.valueOf(system.today())
		);

		insert tkObj;
		return tkObj;
	}

	// create Multiple Task Records
	public List<Task> createMultiTaskRec(Integer i)
	{
		List<Task> tskList = new List<Task>();
		for(Integer j=0; j<i;j++)
		{
			tskList.add(
				new Task(
					Subject = 'Courtesy Call',
					OwnerId = UserInfo.getUserId(),
					Status = 'Not Started',
					ActivityDate = system.today(),
					Priority = 'Medium',
					Activity_Date__c = String.valueOf(system.today())
				)
			);
		}

		if(!tskList.isEmpty())
			insert tskList;

		return tskList;
	}

	// create a close case reminder task
	public Task createClsCaseRemindTaskRec(ID ObjectId)
	{
		return new Task(
			Subject = 'Close Case Reminder',
			OwnerId = UserInfo.getUserId(),
			Status = 'Not Started',
			ActivityDate = system.today(),
			Priority = 'Low',
			Activity_Date__c = String.valueOf(system.today()),
			WhatId = ObjectId
		);
	}


	// create a Single Test User
	public User createTestUsr()
	{
		return createTestUsr('System Administrator', 'TestFirst', 'TestLast');
	}


	// create a Single Test User
	public User createTestUsr(String profileName, String first, String last)
	{
		Profile p = [
			SELECT
				id, Name
			FROM
				Profile
			WHERE
				Name =: profileName
			LIMIT 1
		];

		User u = new User(
			CompanyName = 'TestComp',
			FirstName = first,
			LastName = last,
			LocaleSidKey = 'en_CA',
			UserName = 'testfirst.last@test.org',
			TimeZoneSidKey = 'America/Indiana/Indianapolis',
			Email = 'testfrt.last@test.org',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			alias = 'test5',
			ProfileId = p.Id
		);

		insert u;
		return u;
	}

	// create a Single Event Record
	public Event createEventRec()
	{
		Event evtObj = new Event(
			Subject = 'Pre-Appointment Review',
			OwnerId = UserInfo.getUserId(),
			DurationInMinutes = 10,
			ActivityDate = null,
			ActivityDateTime = system.now(),
			Type = 'Other',
			Activity_Date__c = String.valueOf(system.today())
		);

		insert evtObj;
		return evtObj;
	}

	// create Multiple Event Records
	public List<Event> createMultiEvtRec(Integer i)
	{
		List<Event> evtList = new List<Event>();

		for(Integer j=0; j< i; j++)
		{
			evtList.add(
				new Event(
					Subject = 'Pre-Appointment Review',
					OwnerId = UserInfo.getUserId(),
					ActivityDateTime = system.now(),
					DurationInMinutes = 10,
					Type = 'Other',
					Activity_Date__c = String.valueOf(system.today())
				)
			);
		}

		if(!evtList.isEmpty())
			insert evtList;

		return evtList;
	}

	public Contact createContactRec()
	{
		Contact c = new Contact(
			FirstName = 'Test1',
			LastName = 'LastTest1',
			LanguagePreference__c = 'English',
			Preferred_Contact_Method__c = 'Email'
		);

		insert c;
		return c;
	}

	public Contact createContactRecWithSourceRef()
	{
		Contact c = new Contact(
			FirstName = 'Test1',
			LastName = 'LastTest1',
			LanguagePreference__c = 'English',
			Preferred_Contact_Method__c = 'Email',
			SourceRefID__c='ref123456789'
		);

		insert c;
		return c;
	}

	public Case createCaseRec(Contact c)
	{

		System.debug('starting createCaseRec');

		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = false,
			Order_Review__c = false,
			Pre_Appointment_Review__c = false,
			Source_Type__c = null
		);

		insert cs;

		System.debug('after inserting in createCaseRec');

		return cs;
	}
	//Deepak Malkani : Method used to create contact with a defined owner
	public Case createCaseRec(Contact c, User u)
	{
		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(), 
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Outcome_Review_Due_Date__c = system.now(),
			Follow_up__c = false,
			Order_Review__c = false,
			Pre_Appointment_Review__c = false,
			Source_Type__c = null,
			OwnerId = u.Id
		);

		insert cs;
		return cs;
	}

	//Deepak Malkani : Method used to create contact with a defined owner
	public List<Case> createMultiCaseRec(Contact c, User u, Integer j)
	{
		List<Case> caseList = new List<Case>();
		for(Integer i=0; i<j ;i++)
		{
			Case cs = new Case(
				ContactId = c.Id,
				Origin = 'Email',
				Priority = 'High',
				Status = 'Open',
				Type = 'Sign up for a new product/service',
				Reason_Code__c = 'Test1',
				Line_of_Business__c = 'FFH',
				Subject = 'Test 1'+i,
				Bill_Cycle__c = 14,
				Close_Review__c = true,
				Close_Review_Due_Date__c = system.now(), 
				Can_Be_Reached__c = '2224443434',
				Outcome_Review__c = true,
				Outcome_Review_Due_Date__c = system.now(),
				Follow_up__c = false,
				Order_Review__c = false,
				Pre_Appointment_Review__c = false,
				Source_Type__c = null,
				OwnerId = u.Id
			);
			caseList.add(cs);
		}
		if(!caseList.isEmpty())
			insert caseList;
		return caseList;
	}

	public List<Case> createopenCaseswtNoTasks(Integer i)
	{
		List<Case> caseInsList = new List<Case>();
		for(Integer j=0; j< i; j++)
		{
			caseInsList.add(
				new Case(
					Origin = 'Email',
					Priority = 'High',
					Status = 'Open',
					Type = 'Sign up for a new product/service',
					Reason_Code__c = 'Test1',
					Line_of_Business__c = 'FFH',
					Subject = 'Test '+j,
					Close_Review__c = false,
					Bill_Cycle__c = 14,
					Close_Review_Due_Date__c = system.now(),
					Can_Be_Reached__c = '2224443434',
					Outcome_Review__c = false,
					Follow_up__c = false,
					Order_Review__c = false,
					Pre_Appointment_Review__c = false
				)
			);
		}

		if(!caseInsList.isEmpty()) insert caseInsList;

		return caseInsList;
	}

	public Case createCaseRecwtOrderReview(Contact c)
	{
		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = false,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = false,
			Source_Type__c = null
		);
		insert cs;
		return cs;
	}

	public Case createCasewithGrp(Contact c)
	{
		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = false,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = false,
			OwnerId = this.mobilityENGroup().Id
		);

		insert cs;
		return cs;
	}

	public Account createAccntRec()
	{
		Account a = new Account(
			Name = 'TestAccnt1',
			Type = 'Mobility',
			Rating = 'Warm',
			Bill_Cycle_Day__c = 12,
			Language_Preference__c = 'English'
		);
		insert a;
		return a;
	}

	public Case createCasewtAcnt(Account a)
	{
		Case cs = new Case(
			AccountId = a.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = '',
			Subject = 'Test 1',
			Close_Review__c = true,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = true,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = false,
			Language_Preference__c = ''
		);

		insert cs;
		return cs;
	}

	public Case createCasewtAcntAllTasks(Account a)
	{
		Case cs = new Case(
			AccountId = a.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = '',
			Subject = 'Test 1',
			Close_Review__c = true,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = true,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = true,
			Language_Preference__c = ''
		);

		insert cs;
		return cs;
	}

	public List<Case> createMultiCaseRec(Integer i, Contact c)
	{
		List<Case> caseList = new List<Case>();
		for(Integer j=0; j< i; j++)
		{
			caseList.add(
				new Case(
					ContactId = c.Id,
					Origin = 'Email',
					Priority = 'High',
					Status = 'Open',
					Type = 'Sign up for a new product/service',
					Reason_Code__c = 'Test1',
					Line_of_Business__c = 'FFH',
					Subject = 'Testing'+i,
					Close_Review__c = false,
					Bill_Cycle__c = 14,
					Close_Review_Due_Date__c = null,
					Can_Be_Reached__c = '2224443434',
					Outcome_Review__c = true,
					Follow_up__c = false,
					Order_Review__c = true,
					Order_Review_Due_Date__c = system.today(),
					Pre_Appointment_Review__c = true
				)
			);
		}
		return caseList;
	}

	public ETLLoads__c setCustomSettings()
	{
		ETLLoads__c CS = new ETLLoads__c();
		CS.runDataLoads__c = true;
		insert CS;
		return CS;
	}

	//Deepak Malkani : Removed from test handler and Added shared methods into Utility Class.
	public List<Task> getAllTasks(Case cse) {
		return [SELECT id, Status, Subject FROM Task WHERE WhatId = :cse.Id];
	}

	public List<Task> getAllTasks(Case cse, String status) {
		return [SELECT id, Status, Subject FROM Task WHERE WhatId = :cse.Id and status = :status];
	}

	public List<Task> filterStatusSubject(List<Task> tasks, String status, String Subject) {
		List<Task> filtered = new List<Task>();
		for (Task t : tasks) {
			if (t.Status == status && t.Subject == subject)
				filtered.add(t);
		}
		return filtered;
	}

	public User createIntegrationUser() {
		User integrationUser = createTestUsr('Integration Profile', 'Integration', 'User1');
		ETLLoads__c customSetting = new ETLLoads__c(SetupOwnerId = integrationUser.Id, runDataLoads__c = true);
		insert customSetting;
		return integrationUser;
	}

	public Group createCaseQueue(User usr, String name) {
		QueueSObject q;
		Group g;
		System.runAs(usr) { // if System.runAs is not used, then you get some nasty errors.
			g = new Group(Type='Queue', Name=name);
			insert g;
			q = new QueueSObject(SobjectType='Case', QueueId=g.Id);
			insert q;	   
		}
		return g;
	}


	//////////////////////////////////////////////////
	/// Private Methods
	//////////////////////////////////////////////////

	private Group mobilityENGroup() {
		return [SELECT id FROM Group WHERE Name = : 'Mobility - EN' LIMIT 1];
	}

	//////////////////////////////////////////////////
	/* 
	Scale Tests Helpers
	
	The method ScaleTestsUnitSize is to use smaller size for scale tests (tests for scalability of the code)

	Issue: Scale tests use large test sample data (e.g. 200 or 300). These tests
	TAKE LONG TIME to run and can slow down development.

	The unit size of the scale unit tests will be 100 if:
	1) In Production
	2) In Unregistered sandbox (see below var: registeredSandboxIds)
	3) Randomly every about 10 times
	4) If set by deverlopers using (setScaleTestsUnitSize(?))

	OTHERWISE (if none of the conditions above are met) the size will be 2.
	*/
	//////////////////////////////////////////////////

	private static Integer privateScaleTestsUnitSize = null;
	public static void setScaleTestsUnitSize(Integer size) {
		privateScaleTestsUnitSize = size;
	}
	public static Integer getScaleTestsUnitSize() {
		try {
			if (privateScaleTestsUnitSize==null) {
				
				Id orgId = UserInfo.getOrganizationId();
				Set<Id> registeredSandboxIds = new Set<Id> {
					'00Dq0000000BSFCEA4' // DEV410
				};

				Boolean isSandbox = registeredSandboxIds.contains(orgId);
				Boolean randomlyEvery10Times = Math.random()>.9;

				privateScaleTestsUnitSize = !isSandbox || randomlyEvery10Times ? 100 : 5;
			}

		} catch (Exception ex) {
			// it it fails for any reason (and it shouldn't) the just return 100
			privateScaleTestsUnitSize = 100;
		}

		// if the value was not set for any reason, just make it 100 to avoid error in the calling code
		if (privateScaleTestsUnitSize==null)
			privateScaleTestsUnitSize = 100;

		return privateScaleTestsUnitSize;
	}	

}