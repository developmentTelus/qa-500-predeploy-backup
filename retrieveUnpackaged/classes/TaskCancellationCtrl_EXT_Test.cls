/*
  Author : Praveen Kumar Bonalu.
  Created Date : Feb 27 2015
  Purpose : Provides Unit Testing for all Business Logic covered under TaskCancellationCrtl_EXT  class
*/

@isTest
public class TaskCancellationCtrl_EXT_Test{

    @isTest static void testUpdate(){

        Task tk = new Task();

        tk.Status = 'Not Started';        
        tk.priority='High'; 
        insert tk;
        
        test.setCurrentPage(Page.Task_Cancellation_Page);
        ApexPages.StandardController con = new ApexPages.StandardController(tk);
        TaskCancellationCrtl_EXT ctc = new TaskCancellationCrtl_EXT(con);
        ctc.t.Reason_Code__c = 'Substituted for custom task';
        ctc.Save();
        system.assertEquals('Substituted for custom task', [SELECT Reason_Code__c FROM Task WHERE id =: tk.Id LIMIT 1].Reason_Code__c);
        system.assertEquals('Cancelled', [SELECT Status FROM Task WHERE id =: tk.Id LIMIT 1].Status);
    }

    @isTest static void testException(){

        Task tk = new Task();
        tk.Status = 'Not Started';        
        tk.priority='High';
        tk.Reason_Code__c = null; 
        insert tk;
        
        test.setCurrentPage(Page.Task_Cancellation_Page);

        ApexPages.StandardController con = new ApexPages.StandardController(tk);
        TaskCancellationCrtl_EXT ctc = new TaskCancellationCrtl_EXT(con);
        ctc.Save();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg: msgs)
        {
            system.assertEquals(true, msg.getDetail().contains('Please provide the Cancelled Reason for this task'));
        }
        system.assertEquals('Not Started',[SELECT Status FROM Task WHERE id =: tk.Id].Status);
    }

}