/*
	Author : Deepak Malkani.
	Created Date : March 16 2015
	Purpose : Test Class for Batch Scheduler - Open Case finder and Open Case Reminder Task
*/

@isTest
public class openCasefinder_Scheduler_Test {
	
	
	static testMethod void UnitTest1() {
		
		String CRON_EXPR = '0 0 02 * * ?';
		Test.startTest();
		String jobId = system.schedule('Open Cases job Test', CRON_EXPR, new openCasefinder_Scheduler());
		// Get the information from the CronTrigger API object
      	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
         					FROM CronTrigger 
         					WHERE id = :jobId];
      	// Verify the expressions are the same
      	System.assertEquals(CRON_EXPR, ct.CronExpression);
      	 // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
      	Test.stopTest();
	}
}