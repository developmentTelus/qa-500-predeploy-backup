/*
    Author : Deepak Malkani.
    Created Date : Feb 10 2015
    Purpose : Purpose of this Controller Extension is to process all Business Logic related to Event History VF Page.
*/
public with sharing class EventHistoryCtrl_Ext {

    public Event evntObj {get; set;}
    public transient List<Task_and_Event_History__c> tskEventHistoryList {get; set;}
    
    public EventHistoryCtrl_Ext(ApexPages.StandardController controller) {
        evntObj = (Event) controller.getRecord();
    }
    
    public List<Task_and_Event_History__c> getEventHistory()
    {
        tskEventHistoryList = [SELECT id, Action__c, Date__c, TaskorEventId__c, User__c FROM Task_and_Event_History__c WHERE TaskorEventId__c = : evntObj.Id ORDER BY Date__c DESC];
        return tskEventHistoryList;
    }

}