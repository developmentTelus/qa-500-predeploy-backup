/*
    Author : Deepak Malkani.
    Created Date : Feb 11 2015
    Purpose : Provides Unit Testing for all Business Logic covered under TaskTriggerHandler class
    
    Deepak Malkani : Adding this comment to test Bitbucket versioning in a Maint Release
*/

@isTest(SeeAllData=false)
public class TaskTriggerHandlerTest
{
    //Test Method used to create a single task and make field changes
    public static testMethod void UnitTest1()
    {
        //Initialise handlers
        UtlityTest testHandler = new UtlityTest();
        List<Task_and_Event_History__c> tskevtHistList = new List<Task_and_Event_History__c>();
        Task tkObj;
        //Start Testing
        Test.startTest();
        tkObj = testHandler.createTaskRec();
        tkObj.Status = 'In Progress';
        tkObj.Activity_Date__c = String.valueOf(system.today()+10);
        tkObj.ActivityDate = system.today() + 10;
        update tkObj;
        
        Test.stopTest();
        //Make Assertions
        tskevtHistList = [SELECT id, Action__c FROM Task_and_Event_History__c WHERE TaskorEventId__c =: tkObj.Id];
        system.assertEquals(3, tskevtHistList.size());
    }

    public static testMethod void UnitTest2()
    {
        //Initialise handlers
        UtlityTest testHandler = new UtlityTest();
        List<Task_and_Event_History__c> tskevtHistList = new List<Task_and_Event_History__c>();
        List<Task> tkUpdList = new List<Task>();
        List<Task> tkInsList = testHandler.createMultiTaskRec(10);
        //start testing
        Test.startTest();
        for(Integer i=0; i<tkInsList.size(); i++)
        {
            Task tkObj = new Task(Id = tkInsList[i].Id);
            tkObj.Status = 'In Progress';
            tkUpdList.add(tkObj);
        }
        if(!tkUpdList.isEmpty())
            update tkUpdList;
        Test.stopTest();
        
        //Make Assertions
        tskevtHistList = [SELECT id, Action__c FROM Task_and_Event_History__c];
        system.assertEquals(10, tskevtHistList.size());
    }

    public static testMethod void UnitTest3()
    {
        //Initialise handlers
        UtlityTest testHandler = new UtlityTest();
        List<Task_and_Event_History__c> tskevtHistList = new List<Task_and_Event_History__c>();
        List<Task> tkUpdList = new List<Task>();
        List<Task> tkInsList = testHandler.createMultiTaskRec(200);
        User u;
        //start testing
        Test.startTest();
        u = testHandler.createTestUsr(); 
        System.runAs(u)
        {
            for(Integer i=0; i<tkInsList.size(); i++)
            {
                Task tkObj = new Task(Id = tkInsList[i].Id);
                tkObj.OwnerId = u.Id;
                tkUpdList.add(tkObj);
            }
            if(!tkUpdList.isEmpty())
                update tkUpdList;
        }
        Test.stopTest();
        
        //Make Assertions
        tskevtHistList = [SELECT id, Action__c FROM Task_and_Event_History__c];
        system.assertEquals(200, tskevtHistList.size()); 
    } 
}