/*
###########################################################################
# File..................: CustomerHierarchyCtrl.cls
# Created by............: TechM - Nilam Patel
# Created Date..........: 16-Jun-2016
# Description...........: this file will do things
#
# Copyright (c) Tech Mahindra. All Rights Reserved.
#
# Created by the Tech Mahindra. Modification must retain the above copyright #notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Bell Mobility, is hereby forbidden. Any modification to #source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# *********** VERSION UPDATES ***********************************************************************
#

# 16-Jun-2016    Nilam Patel        Created File
#
# ***************************************************************************************************
# Internal Coding Guidelines: https://project.collaborate.tsl.telus.com/sites/outcome/SitePages/Coding%20Guidelines.aspx
# ***************************************************************************************************
*/



public  class CustomerHierarchyCtrl {
    
    public List<Account> accountList {get;set;}
    public List<CustomerWrapper> CustomerWrapList {get;set;} 
    public List<Task> taskList {get; set;}
    public List<CaseComment> caseCommentList {get; set;}
    public List<Case> latestCase {get;set;}    
    public List<Case> srchCaseList {get;set;}

    public Case caseDefault {get;set;}
    public Case ticketCase {get;set;}
    public Account objError {get;set;}
    public User objUser {get;set;}



    public String parBAN {get;set;}
    public String parCAN {get;set;}
    public String parTID {get;set;}
    public String parCNo {get;set;}
    public String parPNo {get;set;}
    public String taskIdToEdit {get; set;}
    public String srchFlag {get;set;}
    public String billingAccntNo {get;set;}
    public String ticketOrderId {get;set;}    
    public String caseNumber {get;set;}        
    public String phoneNumber {get;set;}  
    public String billingAddress {get;set;}
    public String baName {get;set;}  
    public String loginID {get;set;}
    public String pageName {get;set;}
    public String CAccountJSON {get;set;}
    public String BAccountJSON {get;set;}
    public String ContactJSON {get;set;}
    public String CaseJSON {get;set;}
    public String ProductJSON {get;set;} 
    public String srcMsgString {get;set;}    

    public ID customerId {get;set;}
    public ID currentCaseId {get;set;}
    public ID savedTaskID {get;set;}
    public ID recentCaseID {get;set;}

    public Boolean showDefaultInactives {get;set;}
    public Boolean cmtSuccess{get;set;}
    public Boolean showPanel {get {if (showPanel == null) showPanel = true; return showPanel; } set;} 
    public Boolean showSearchPanel {get {if (showSearchPanel == null) showSearchPanel = false; return showSearchPanel; } set;} 
    public Boolean showErrorPanel {get;set;}
    public Boolean showPhonePanel {get;set;}
    public Boolean noRecFlag {get;set;}
    public Boolean showPageErrorPanel {get;set;}

    


    public CaseComment addCaseComment {
        get { if (addCaseComment == null)
                addCaseComment = new CaseComment();
              return addCaseComment;
        }
        set;
      }
    public Task aeTask {
        get { if (aeTask == null)
                aeTask = new Task();
            return aeTask;
        }
        set;
      }
    


    Map<Id,Task> mapIdToTask {get; set;}
    static Map<String, String> shortCTN = new Map<String, String>();
    static {
                shortCTN.put('Sign up for a new product/service', 'Sign up');
                shortCTN.put('Learn about a product/service', 'Learn about');
                shortCTN.put('Get a better deal', 'Better deal');
                shortCTN.put('Move my service(s)', 'Move service');
                shortCTN.put('Get my product/services working', 'Fix Service(s)');
                shortCTN.put('Make changes to my existing acct/service', 'Change service(s)');
                shortCTN.put('Inquire, dispute or pay my bill', 'Billing Inquire');
                shortCTN.put('Cancel my service(s)', 'Cancel service(s)');
                shortCTN.put('Give feedback', 'Give feedback');
    }




    public CustomerHierarchyCtrl() {

        try {
    
                loginID = UserInfo.getUserId();    
                mapIdToTask = new Map<Id,Task>(); 
                
                showErrorPanel = false;
                showPageErrorPanel = false;
                noRecFlag = false;
                getProfileInfo();
                parBAN = ApexPages.currentPage().getParameters().get('ban');
                parCAN = ApexPages.currentPage().getParameters().get('can');
                parTID = ApexPages.currentPage().getParameters().get('tid');
                parCNo = ApexPages.currentPage().getParameters().get('casenum');
                parPNo = ApexPages.currentPage().getParameters().get('phonenum');
                srchFlag = ApexPages.currentPage().getParameters().get('search');

                if(!String.isEmpty(parBAN))
                {
                    customerId = getParentIdByBAN(parBAN);
                    srcMsgString = 'BAN #: ' + parBAN;
                }
                else if(!String.isEmpty(parCAN))   
                {
                    customerId = getParentIdByCAN(parCAN); 
                    srcMsgString = 'CAN #: ' + parCAN;
                }
                else if(!String.isEmpty(parCNo))       
                {
                    customerId = getParentIdByCNo(parCNo);
                    srcMsgString = 'Case Number: ' + parCNo;
                }
                else if(!String.isEmpty(parTID) )       
                {
                    customerId = getParentIdByTID(parTID);  
                    srcMsgString = 'Ticked ID: ' + parTID;
                }
                else if(!String.isEmpty(parPNo)) 
                {
                    
                    showSearchPanel = true;
                    showPhonePanel = true;
                    showPanel = false;
                    pageName = 'cpCustomer?';
                    srcMsgString = 'Phone #: ' + parPNo;
                    srchCaseList = getCasesbyPhone(parPNo);
                    if(srchCaseList !=null )
                        return;  
                    else
                        noRecFlag = true;
                     
                }
                else
                     noRecFlag = true;


                if(!String.isEmpty(customerId) && String.isEmpty(parPNo))
                {
                        CustomerWrapList = new List<CustomerWrapper>();
                        CustomerWrapList.add(new CustomerWrapper(customerId));
                        caseDefault = new Case();

                        if(!String.isEmpty(parTID)  || !String.isEmpty(parCNo) )
                            caseDefault = ticketCase;
                        else
                        {
                           if(!CustomerWrapList[0].contactIds.isEmpty())
                                caseDefault =  [SELECT ID, Status FROM Case Where ContactID IN :CustomerWrapList[0].contactIds ORDER BY Status asc, CaseNumber desc LIMIT 1];
                        }

                        buildCAccountJSON();
                        buildBAccountJSON();
                        buildContactJSON();
                        buildCaseJSON();
                        //buildProductJSON();
                } 
                else
                    noRecFlag = true; 

               if(noRecFlag)
               {
                    showSearchPanel = true;
                    showErrorPanel = true;
                    showPhonePanel = false;
                    showPanel = false;
                    
                    if(!String.isEmpty(srcMsgString))
                    {
                        objError = new Account();
                        objError.addError('No result found for ' + srcMsgString);
                    }
                    //else
                        //objError.addError('No result found. Please use "Search" for the information you are looking for.');
               }  

        }
        catch (Exception e) 
        {
            showPageErrorPanel = true;
            objError = new Account();
            objError.addError('Error Message: ' + e.getMessage());
            objError.addError('Line Number: ' + e.getLineNumber());
            objError.addError('Error Trace: ' + e.getStackTraceString());
            objError.addError('Error Type: ' + e.getTypeName());

        }

    }

    public String getsrcMsgString()
    {
        return srcMsgString;
    }



    private void getProfileInfo()
    {
        String userId;
        objUser = new User();
        userId = UserInfo.getUserId();
        objUser =  [Select Profile.Name From User where id =: userId LIMIT 1];
    }   


    private ID getParentIdByCAN(String can)
    {
        List<Account> tempA= new List<Account>();
        tempA =  [SELECT id FROM Account WHERE SourceRefId__c =: 'C_'+ can AND Type = 'Customer' LIMIT 1];
        if(!tempA.isEmpty())
            return tempA[0].id;
        else
            return null; 
    }   

    private ID getParentIdByBAN(String ban)
    {
        List<Account> tempA= new List<Account>();
        tempA =  [SELECT ParentId FROM Account WHERE Billing_Account_Number__c =:ban limit 1];
        if(!tempA.isEmpty())
            return tempA[0].ParentId;
        else
            return null;    
    }  

    private ID getParentIdByTID(String tid)
    {
        List<Case> tempC= new List<Case>();
        ID tempA;
        ID tempId;
        tempC = [SELECT ContactId, Id, Status FROM Case WHERE Order_Ticket_ID__c =: tid  LIMIT 1];
        
        if(!tempC.isEmpty())
        {
            tempId = tempC[0].contactId;
            ticketCase = tempC[0];
            tempA = [SELECT AccountId__c FROM AccountContacts__c WHERE ContactId__c =: tempId LIMIT 1].AccountId__c;
            return tempA;
        } 
        else
            return null;     
    }   

    private ID getParentIdByCNo(String cno)
    {
        List<Case> tempC= new List<Case>();
        ID tempA;
        ID tempId;
        tempC = [SELECT ContactId, Id, Status FROM Case WHERE  CaseNumber =: cno  LIMIT 1];
        if(!tempC.isEmpty())
        {
            tempId = tempC[0].contactId;
            ticketCase = tempC[0];
            tempA = [SELECT AccountId__c FROM AccountContacts__c WHERE ContactId__c =: tempId LIMIT 1].AccountId__c;
            return tempA;
        }
        else
            return null;  
    } 

    public List<Case> getCasesbyPhone(String phoneNumber)
    {
            List <String> phoneFormats = new List<String>();
            List<Case> tmpList = new List<Case>();
            

            if(phoneNumber != null && phoneNumber != '')
            {
              phoneFormats = buildSrchPhones(phoneNumber);
              tmpList = [SELECT Id, Status ,CaseNumber, Type, Line_of_Business__c, Contact.Name, Owner.Name, Lastmodifieddate
                         FROM Case
                         WHERE Contact.HomePhone IN:phoneFormats   OR  Contact.Phone IN:phoneFormats  OR Contact.OtherPhone IN:phoneFormats 
                         ORDER BY Lastmodifieddate DESC, CaseNumber DESC];
            }

            if(!tmpList.isEmpty())
                return tmpList;
            else
                return null;
    } 


    public List<String> buildSrchPhones(String phNum)
    {
        List <String> tempList = new List<String>();
        String ptrn;
        phNum = phNum.replaceAll('[^0-9]', '');
        tempList.add(phNum);
        ptrn  = '(' + phNum.left(3) + ') ' + phNum.mid(3, 3 ) + '-' + phNum.right(4);
        tempList.add(ptrn);
        ptrn  =  phNum.left(3) + '-' + phNum.mid(3, 3 ) + '-' + phNum.right(4); 
        tempList.add(ptrn);
        return tempList;
    }

    public class CustomerWrapper
    {
        transient public List<Account> customerList {get;set;}
        transient public List<Account> bAccountList {get;set;}
        transient public List<Asset> productList {get;set;}
        transient public List<Contact> contactList {get;set;}
        public List<String> contactIds = new List<String>();
        public List<contactWrapper> contactWrapList = new List<contactWrapper>(); 

        public CustomerWrapper(ID customerID)
        {
            this.customerList = new List<Account>();
            this.bAccountList = new List<Account>();
            this.productList = new List<Asset>();
            this.contactList = new List<Contact>();

            this.customerList = [SELECT ID, Name, Brand__c, Language_Preference__c, Bill_Cycle_Day__c, BillingAddress, Type, BillingStreet, BillingCity, BillingState, BillingCountry,BillingPostalCode FROM Account WHERE ID = : customerID];
            this.bAccountList = [SELECT ID, Name, Billing_Account_Number__c, Brand__c, Next_Billing_Date__c, Status__c, Bill_Cycle_Day__c, BillingAddress, Type, BillingStreet, BillingCity, BillingState, BillingCountry,BillingPostalCode  FROM Account WHERE ParentId = : customerID Order By Name];
            this.productList =  [SELECT ID, AccountId, Description, InstallDate, Name FROM Asset WHERE AccountId = : customerID ];
            this.contactList =  [SELECT ID, FirstName, Lastname, HomePhone, OtherPhone, Phone, Preferred_Contact_Method_formula__c, Email_Address__c FROM Contact WHERE ID in (SELECT ContactId__c FROM AccountContacts__c WHERE AccountId__c =: customerID) Order By FirstName];
           
            if(!this.contactList.isEmpty())
            {
                for (Contact cnt: this.contactList)
                {
                    this.contactWrapList.add(new contactWrapper(cnt));
                    this.contactIds.add(cnt.Id);
                }
            }    

        }   
       public List<contactWrapper> getcontactWrapList()
        {
            return contactWrapList;
        }    
    }



    public class contactWrapper
    {
        transient public Contact contactWrap {get;set;}
        transient public List<Case> caseList {get;set;}
        public List<caseWrapper> caseWrapList = new List<caseWrapper>(); 


        public contactWrapper(Contact objContact)
        {
            this.caseList = new List<Case>();
            this.contactWrap =  objContact; 
            this.caseList = [SELECT ID, CaseNumber , ContactId, Type, Status, OwnerID, Owner.Name, CreatedDate, Lastmodifieddate, Line_of_Business__c, ParentId, AccountId, (Select ID, CaseNumber , ContactId, Type, Status, OwnerID, Owner.Name, CreatedDate, Lastmodifieddate, Line_of_Business__c, ParentId, AccountId From Cases Order By CaseNumber desc )  FROM Case WHERE ContactId =: objContact.ID Order By CaseNumber desc];
            
            if(!this.caseList.isEmpty())
            {
                for (Case cnt: this.caseList)
                {
                    if(cnt.ParentId == null)
                        this.caseWrapList.add(new caseWrapper(cnt));
                }
            }
        }

        public List<caseWrapper> getcaseWrapList()
        {
            return caseWrapList;
        }

    }

    public class CaseWrapper
    {
        transient public Case caseWrap {get;set;}
        public ID CaseID {get;set;}
        public String CaseType {get;set;}
        public List<Case> childCaseList {get;set;}
        public List<cldCaseWrapper> childCaseWrapList = new List<cldCaseWrapper>(); 

        public CaseWrapper(Case objCase)
        {
            this.caseWrap =  objCase; 
            this.CaseID = objCase.ID;
            this.CaseType = shortCTN.get(objCase.Type);  
            
            this.childCaseList = objCase.cases;
            
            for (Case cnt: this.childCaseList)
            {
                    this.childCaseWrapList.add(new cldCaseWrapper(cnt));
            }            

        }

        public List<cldCaseWrapper> getchildCaseWrapList()
        {
            return childCaseWrapList;
        }

    }

    public class cldCaseWrapper
    {
        transient public Case cCaseWrap {get;set;}
        public ID CaseID {get;set;}
        public String CaseType {get;set;}
        
        
        public cldCaseWrapper(Case objCase)
        {
            this.cCaseWrap =  objCase; 
            this.CaseID = objCase.ID;
            this.CaseType = shortCTN.get(objCase.Type); 
        }

    }




  public List<CustomerWrapper> getCustomerHierarchy()
    {
        return CustomerWrapList;
    } 


   public void buildCAccountJSON()
   {
        String caccountStr = '{ ';

        for(Account cnt: CustomerWrapList[0].customerList)
        {   
            caccountStr =  caccountStr + '"' + cnt.ID + '": {';
            caccountStr =  caccountStr + '"name": "' + strClr(cnt.Name) + '",';
            caccountStr =  caccountStr + '"lang":"' + cnt.Language_Preference__c + '",';
            caccountStr =  caccountStr + '"brand":"' + cnt.Brand__c + '",';
            caccountStr =  caccountStr + '"bcd":"' + cnt.Bill_Cycle_Day__c + '",';
            caccountStr =  caccountStr + '"madd":"' + strClr(cnt.BillingStreet) + '<br />' +  strClr(cnt.BillingCity) + ', ' + strClr(cnt.BillingState) + '&nbsp;' +  strClr(cnt.BillingPostalCode) + '<br />' +  strClr(cnt.BillingCountry) + '"},';
        }
        caccountStr = caccountStr.subString(0,caccountStr.Length()-1);
        caccountStr =  caccountStr + ' }';
        CAccountJSON = caccountStr;
   }   

   public void buildBAccountJSON()
   {
        String baccountStr = '{ ';

        for(Account cnt: CustomerWrapList[0].bAccountList)
        {   
            baccountStr =  baccountStr + '"' + cnt.ID + '": {';
            baccountStr =  baccountStr + '"name": "' + strClr(cnt.Name) + '",';
            baccountStr =  baccountStr + '"ban":"' + strClr(cnt.Billing_Account_Number__c) + '",';
            baccountStr =  baccountStr + '"brand":"' + cnt.Brand__c + '",';
            baccountStr =  baccountStr + '"bcd":"' + cnt.Next_Billing_Date__c + '",';
            baccountStr =  baccountStr + '"status":"' + strClr(cnt.Status__c) + '",';
            baccountStr =  baccountStr + '"madd":"' + strClr(cnt.BillingStreet) + '<br />' +  strClr(cnt.BillingCity) + ', ' + strClr(cnt.BillingState) + '&nbsp;' +  strClr(cnt.BillingPostalCode) + '<br />' +  strClr(cnt.BillingCountry) + '"},';
        }
        baccountStr = baccountStr.subString(0,baccountStr.Length()-1);
        baccountStr =  baccountStr + ' }';

        BAccountJSON = baccountStr;
        
   } 


   public void buildContactJSON()
   {
        String contactStr = '{ ';
        for(Contact cnt: CustomerWrapList[0].contactList)
        {   
            contactStr =  contactStr + '"' + cnt.ID + '": {';
            contactStr =  contactStr + '"name": "' + strClr(cnt.FirstName) + '&nbsp;' + strClr(cnt.Lastname) + '",';
            contactStr =  contactStr + '"eve":"' + strClr(cnt.HomePhone) + '",';
            contactStr =  contactStr + '"day":"' + strClr(cnt.Phone) + '",';
            contactStr =  contactStr + '"work":"' + strClr(cnt.OtherPhone) + '",';
            contactStr =  contactStr + '"email":"' + strClr(cnt.Email_Address__c) + '",';
            contactStr =  contactStr + '"method":"' + strClr(cnt.Preferred_Contact_Method_formula__c) + '"},';
        }
        contactStr = contactStr.subString(0,contactStr.Length()-1);
        contactStr =  contactStr + ' }';

        ContactJSON = contactStr;
        
   } 

   public void buildCaseJSON()
   {
        String ccStr = '{ ';
        for(contactWrapper cntp: CustomerWrapList[0].contactWrapList)
        {   

            for(Case cnt: cntp.caseList)
            {
                ccStr +=  '"' + cnt.ID + '": {';
                ccStr +=  '"number": "' + strClr(cnt.CaseNumber) + '",';
                ccStr +=  '"outcome":"' + strClr(cnt.Type) + '",';
                ccStr +=  '"status":"' + strClr(cnt.Status) + '",';
                ccStr +=  '"owner":"' + strClr(cnt.Owner.Name) + '",';
                ccStr +=  '"dopened":"' + dtFrm(cnt.CreatedDate) + '",';
                ccStr +=  '"lmodified":"' + dtFrm(cnt.Lastmodifieddate) + '",';
                ccStr +=  '"lob":"' + strClr(cnt.Line_of_Business__c) + '",';
                ccStr +=  '"actid":"' + strClr(cnt.AccountId) + '",';
                ccStr +=  '"pcaseid":"' + strClr(cnt.ParentId) + '",';
                ccStr +=  '"contactid":"' + strClr(cnt.ContactId) + '"},';
            }
        }
        ccStr = ccStr.subString(0, ccStr.Length()-1);
        ccStr +=  ' }';
        CaseJSON = ccStr;

   }

/* 
  public void buildProductJSON()
   {
        String productStr = '{ ';
        for(Asset cnt: CustomerWrapList[0].productList)
        {   
            productStr =  productStr + '"' + cnt.ID + '": {';
            productStr =  productStr + '"type": "coming soon...",';
            productStr =  productStr + '"subname":"coming soon...",';
            productStr =  productStr + '"tn":"' + strClr(cnt.Name) + '",';
            productStr =  productStr + '"sdate":"' + dtFrm(cnt.InstallDate) + '",';
            productStr =  productStr + '"cdate":"coming soon...",';
            productStr =  productStr + '"srvtype":"' + strClr(cnt.Description) + '"},';
        }
        productStr = productStr.subString(0,productStr.Length()-1);
        productStr =  productStr + ' }';
        ProductJSON = productStr;
   } 

*/

  public List<Case> getCaseById()
  {

    List<Case> tmpList = new List<Case>();
    billingAddress = '';
    String caseId = ApexPages.currentPage().getParameters().get('caseId');
    if(caseId == null)
    {
        caseId = caseDefault.id;
    }
    if(caseId != null)
    {
        currentCaseId = caseId;
        tmpList = [SELECT ID, CaseNumber, ContactId, AccountId, Type, Line_of_Business__c, Status, OwnerId, Owner.Name, Owner.Phone, CreatedDate, Lastmodifieddate,  Notification__c, Notification_ID__c, Order_Ticket_ID__c, Related_Order_Ticket__c, Billing_Account_Number__c, Due_Date__c, Description, Contact.Name, Contact.Email, Contact.homephone, Contact.mobilephone, Contact.phone, Contact.LanguagePreference__c, Account.Name, Account.Status__c, Account.Brand__c, Account.Next_Billing_Date__c, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingCountry, Account.BillingPostalCode  FROM Case WHERE ID =: caseId];        
        
        billingAddress = tmpList[0].Account.BillingStreet + ', ' + tmpList[0].Account.BillingCity + ', ' + tmpList[0].Account.BillingState + ' ' + tmpList[0].Account.BillingPostalCode  ;
        baName = tmpList[0].Account.Name;     
        return tmpList;
    }    
    return null;
  }    


  public List<CaseComment> getCommentsByCaseId()
  {
        List<CaseComment> tmpList = new List<CaseComment>();
        String caseId = ApexPages.currentPage().getParameters().get('caseId');
        if(caseId == null)
            caseId = caseDefault.id;
        tmpList =  [SELECT CommentBody,CreatedById, CreatedBy.Name, CreatedBy.Phone ,CreatedDate,Id,LastModifiedById,LastModifiedDate,ParentId FROM CaseComment WHERE ParentId =: caseId order by LastModifiedDate desc];
        
        caseCommentList = tmpList;
        return tmpList;            
  }

  public String getCaseComUserJSON()
   {
        
        String ccrStr = '{ ';
        String strtemp = '';
        List<User> tmpList;
        List<String> userIdList = new List<String>();

        for(CaseComment cnt: caseCommentList)
        {   
            userIdList.add(cnt.CreatedById); 
        }         
        tmpList = [SELECT Id, Name, Phone, Street, State, City, PostalCode, Country, Alias , Profile.Name FROM User WHERE Id IN :userIdList];
        
        for(User cnt: tmpList)
        {   
            strtemp =  strClr(cnt.Street) + '<br />' + strClr(cnt.City) + ' ' + strClr(cnt.State) + ' ' + strClr(cnt.PostalCode) + '<br />' + strClr(cnt.Country); 
            ccrStr +=  '"' + cnt.ID + '": {';
            ccrStr +=  '"name": "' + strClr(cnt.Name) + '",';
            ccrStr +=   '"pin":"' + strClr(cnt.Alias) + '",';
            ccrStr +=  '"org":"coming soon...",';
            ccrStr +=   '"tn":"' + strClr(cnt.Phone) + '",';
            ccrStr +=   '"orgid":"coming soon...",';
            ccrStr +=   '"roleFunc":"' + strClr(cnt.Profile.Name) + '",';
            ccrStr +=   '"otype":"coming soon...",';
            ccrStr +=  '"sadd":"' + strtemp + '"},';
        }
        ccrStr = ccrStr.subString(0,ccrStr.Length()-1);
        ccrStr += ' }';
        return ccrStr;
   } 


  public Integer getTasksByCaseId()
  {
        List<Task> tmpList = new List<Task>();
      
        String caseId = ApexPages.currentPage().getParameters().get('caseId');
        if(caseId == null)
            caseId = caseDefault.id;
        mapIdToTask = new Map<Id,Task>([SELECT ActivityDate,Description,Id,OwnerId,Owner.Name, Priority,Status,Subject,Type, IsClosed, CreatedById, WhatId FROM Task WHERE WhatId =: caseId order by ActivityDate desc]);
        tmpList.addAll(mapIdToTask.values());
      	taskList = tmpList;
        return tmpList.size();      

  }

  public Integer getTotalSearchCases()
  {
    if(srchCaseList != null )
        return srchCaseList.size();
    else
        return 0;
  }

  public String getTaskJSON()
   {

        if(!taskList.isEmpty())
        {
            String taskStr = '{ ';
            for(Task cnt: taskList)
            {   
                taskStr +=  '"' + cnt.ID + '": {';
                taskStr +=  '"subject": "' + strClr(cnt.Subject) + '",';
                taskStr +=   '"priority":"' + cnt.Priority + '",';
                taskStr +=  '"dueDt":"' + strClr(cnt.ActivityDate == null ? '' : cnt.ActivityDate.format()) + '",';
                taskStr +=  '"cmt":"' + strClr(cnt.Description) + '"},';
            }
            taskStr = taskStr.subString(0,taskStr.Length()-1);
            taskStr += ' }';
            return taskStr;
        }
        return null;
   } 




   private String strClr(String str)
   {
        if(str == null)
        {
            return '';
        }

        Map<String, String> htmlEncodingMap = new Map<String, String>();
        htmlEncodingMap.put('"', '&quot;');
        htmlEncodingMap.put('\'', '&#39;');

        for (String token : htmlEncodingMap.keySet()) {
            str = str.replace(token, htmlEncodingMap.get(token))
                .replace('\r','    ')
                .replace('\n','    ');
        }

        return str;
   }

    private String dtFrm(Datetime dt)
    {
        return dt.format('dd/MM/yyyy');
    }


    public PageReference saveCaseComment()
    {
        String parentcaseid =  ApexPages.currentPage().getParameters().get('caseId');
            
            if(String.isBlank(addCaseComment.CommentBody))
            {
                cmtSuccess = false;
                addCaseComment.addError('Please enter case comment.');
                
            }
            else
            {
                addCaseComment.ParentId = parentcaseid;
                insert addCaseComment;
                addCaseComment = null;
                cmtSuccess = true;
            }
        return null;
    }
    



   public PageReference saveTask()
    {
        String sWhatId =  ApexPages.currentPage().getParameters().get('caseId');
        String sOwnerId =  ApexPages.currentPage().getParameters().get('taskOwnerId');
        
        Boolean errorFlag =  false;  
            if(String.isBlank(aeTask.Description))
            {
                aeTask.addError('Please enter description.');
                errorFlag = true;
            }

            if(aeTask.Priority == null )
            {
                aeTask.addError('Please select priority.');
                errorFlag = true;
            }            
        
            if(aeTask.ActivityDate == null )
            {
                aeTask.addError('Please enter due date. Allowed date is today or future date');
                errorFlag = true;
            }
        
            if(aeTask.ActivityDate < System.today() )
            {
                aeTask.addError('Please enter today or future due date.');
                errorFlag = true;
            }

            if(string.valueOf(sOwnerId).startsWith('00G')){
                aeTask.addError('Task cannot be created against queue');
                return null;
            }            
      
            if(!errorFlag)
            {
                 aeTask.Subject = 'Follow Up';
                 aeTask.WhatId = sWhatId;
                 aeTask.OwnerId = sOwnerId;
                 upsert aeTask;
                 savedTaskID = aeTask.Id;
                 aeTask = null;
                 cmtSuccess = true;
                
            }
            else
            {
                 cmtSuccess = false;
            }
                
        return null;
    }

    public PageReference editTaskLink(){
        if(taskIdToEdit != NULL && mapIdToTask.containsKey((Id)taskIdToEdit)){
            aeTask = mapIdToTask.get((Id)taskIdToEdit);
            cmtSuccess = false;
        }
        return null;
    }

    public void clearTaskForm()
    {
        aeTask = new Task();
    }

    public PageReference srchCase()
    {
       
            String strURL;
            strURL = '/cpCustomer?search=true&';
            if(!String.isEmpty(billingAccntNo))
                strURL += 'ban='+ billingAccntNo.trim();
            else if(!String.isEmpty(caseNumber))
                strURL += 'casenum='+ caseNumber.trim();            
            else if(!String.isEmpty(ticketOrderId) )
                strURL += 'tid='+ ticketOrderId.trim();
            else if(!String.isEmpty(phoneNumber))
            {
                phoneNumber = phoneNumber.replaceAll('[^0-9]', '');
                strURL += 'phonenum='+ phoneNumber.trim();
            }
            else
            {
                objError = new Account();
                objError.addError('Please enter a value to search.');
                showErrorPanel = false;
                return null;
            }
            PageReference pageRef = new PageReference(strURL);
            return pageRef;
    }

    public Boolean getShowFeed()
    {
        if(objUser.Profile.Name == 'TELUS Technician')
            return false;
        else    
            return true;
    }

    public Boolean getShowSearch()
    {
        if(objUser.Profile.Name == 'TELUS Technician')
            return false;
        else    
            return true;        
    
    }

    public Boolean getShowRightPanel()
    {
        if(objUser.Profile.Name == 'TELUS Technician')
            return false;
        else    
            return true;        
    }

    public String getCenterPanelWidth()
    {
        if(objUser.Profile.Name == 'TELUS Technician')
            return '80%';
        else    
            return '56%';
    }



}