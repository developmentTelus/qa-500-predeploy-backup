/*
    Author : Dharma Penugonda
    Created Date : 08/12/2015
    Purpose : Post field values to webservice on case insert or update. Jira # CES20-473
*/

public with sharing class CaseTriggerPostHandler {  
    
    public class FetchInit{
        String queryFields = 'Id, accountId, account.parentId, ownerId, contactId, CaseNumber, owner.name, '+
                               'account.name, account.parent.name, contact.name, account.Billing_Account_Number__c, type,' + 
                               'status, due_date__c, CreatedDate, contact.OtherPhone, contact.HomePhone, contact.MobilePhone,'+ 
                               'contact.phone, owner.type, owner.profile.name';   
        List<Id> caseId_User = new List<Id>();
        List<Id> caseId_Queue = new List<Id>();     
    }
    
    public void CaseInsert(List<Case> caseIns){
        FetchInit f = new FetchInit();
        List<Case> caseList = database.query('select ' + f.queryFields + ' from Case where Id IN :caseIns');
            for(Case c : caseList){
                if(c.owner.profile.name != 'Integration Profile' && c.owner.profile.name != 'API User' && c.Status != 'closed'){
                    if(c.owner.type == 'User'){
                        f.caseId_User.add(c.Id);
                    } else if (c.owner.type == 'Queue'){
                        f.caseId_Queue.add(c.id);
                    }
                }         
        	}
        	if(f.caseId_User.size()>0){PostCases_User(f.caseId_User);}
            if(f.caseId_Queue.size()>0){PostCases_Queue(f.caseId_Queue);}
    }

    public void CaseUpdate(List<Case> caseUpd, Map<id, case> oldMap, Map<id, case> newMap){
        FetchInit f = new FetchInit();
        List<Case> caseList1 = database.query('select ' + f.queryFields + ' from Case where Id IN :caseUpd');
        for(Case c : caseList1){  
            if(c.owner.profile.name != 'Integration Profile' && c.owner.profile.name != 'API User' && c.Status != 'closed') {                
                Case oldCase = oldMap.get(c.id);
                Case newCase = newMap.get(c.id);
                     if (c.owner.type == 'User'  && CaseChanged(oldCase, newCase)) f.caseId_User.add(c.id);
                else if (c.owner.type == 'Queue' && CaseChanged(oldCase, newCase)) f.caseId_Queue.add(c.id);
            } 
        }          
        if(f.caseId_User.size()>0){postCases_User(f.caseId_User);}
        if(f.caseId_Queue.size()>0){postCases_Queue(f.caseId_Queue);}     
    }

    private Boolean CaseChanged(Case oldCase, Case newCase) {
        return
            oldCase.Due_Date__c != newCase.Due_Date__c ||
            oldCase.status != newCase.status ||
            oldCase.OwnerId != newCase.OwnerId;
    }
    
    public static String IdSplit(Id i){
        String s = i+'';
        return s.substring(0,s.Length()-3);
    }
    
    public static String FormatJson(String jsonString) {
        if(jsonString != null) {
            jsonString = jsonString
            .replace('00:00:00', '11:59:59') //for due date field
            .replace('null', '-1'); //replace null with -1
        }
        return JsonString;
    }
    
    public class CaseVars {
        public String caseId;
        public String accountId;
        public String parentAccountId;
        public String caseOwnerId;
        public String contactId;
        public String agentId;
        public String caseNumber;
        public String caseOwnerName;
        public String accountName;
        public String parentAccountName;
        public String contactName;
        public String accountBAN;
        public String requestedOutcome;
        public String status;
        public String dueDate; //temporarily marked as string. Should be changed to datetime
        public Datetime createdDate;
        public list<phoneNumbers> contactPhoneNumbers;
        public map<Id, string> assetInfo = New Map<Id, String>();
    }
    
    public class PhoneNumbers {
        public string dayPhone;
        public string eveningPhone;
        public string mobile;
        public string workPhone;
    }
    
    @future(callout=true)
    public static void PostCases_User(List<Id> caseId_User){
        FetchInit f = new FetchInit();
        //Get cases corresponding to passed Ids
        List<Case> userCases = database.query('select ' + f.queryFields + ' from Case where Id IN :caseId_User'); 
        //Instantiate
        List<CaseVars> caseVars = new List<CaseVars>();
        Map<Id, caseVars> caseMap = new Map<Id, CaseVars>();
        List<Id> accountIds = new List<Id>();
        for(Case c: userCases){
            if(c.account.parentId != null){
                accountIds.add(c.account.parentId);
            }
        }

        List<Asset> assetIds = new List<Asset>([SELECT id, accountid, description, serialnumber FROM asset 
                                                WHERE accountid IN :accountIds]);         
        String fedId = [SELECT FederationIdentifier FROM User where id = : UserInfo.getUserId() Limit 1].FederationIdentifier;        
        
        for(Case c: userCases){
            CaseVars caseVar = new CaseVars();
            caseVar.caseId = IdSplit(c.Id); 
            caseVar.accountId = IdSplit(c.accountId);
            caseVar.parentAccountId = IdSplit(c.account.parentId);
            caseVar.caseOwnerId = IdSplit(c.OwnerId);
            caseVar.contactId = IdSplit(c.ContactId);
            caseVar.caseNumber = c.CaseNumber;
            caseVar.caseOwnerName = c.owner.name;
            caseVar.accountName = c.account.name;
            caseVar.parentAccountName = c.account.parent.name;
            caseVar.contactName = c.contact.name;
            casevar.accountBAN = c.account.Billing_Account_Number__c;
            caseVar.requestedOutcome = c.Type;
            caseVar.status = c.Status;
            caseVar.dueDate = c.Due_Date__c+'';//temporary requirement, should be changed to datetime field
            caseVar.createdDate = c.CreatedDate;
            caseVar.agentId = fedId;
            if(c.accountId == null && c.contactId != null){
                caseVar.contactPhoneNumbers = new List<PhoneNumbers>();
                PhoneNumbers p = new PhoneNumbers();
                p.dayPhone = c.contact.otherphone;
                p.eveningPhone = c.contact.homePhone;
                p.mobile = c.contact.mobilePhone;
                p.workPhone = c.contact.phone;
                caseVar.contactPhoneNumbers.add(p);                
            }
            if(c.account.parentId != null){
                for(Asset a : assetIds){
                    if(a.description == 'TN'){caseVar.assetInfo.put(a.id,a.serialnumber);}
                }
            }
            caseMap.put(c.Id, caseVar);
        }
  
        String postJson = formatJson(JSON.serialize(caseMap.values()));
        system.debug(postJson);  

        // Aneeq ---> TODO: post 'postJson' to webservice
    }
    
    @future(callout=true)
    public static void PostCases_Queue(List<Id> caseId_Queue){
        FetchInit f = new FetchInit();
        //Get cases corresponding to passed Ids
        List<Case> queueCases = database.query('select ' + f.queryFields + ' from Case where Id IN :caseId_Queue');        
        //Instantiate
        List<CaseVars> caseVars = new list<CaseVars>();
        Map<Id, CaseVars> caseMap = new Map<Id, CaseVars>();        
        
        for(Case c: queueCases){
            CaseVars caseVar = new caseVars();
            caseVar.caseId = IdSplit(c.Id); 
            caseVar.caseNumber = c.CaseNumber;
            caseVar.caseOwnerName = c.owner.name;
            caseVar.requestedOutcome = c.Type;
            caseVar.status = c.Status;
            caseVar.dueDate = c.Due_Date__c+'';
            caseMap.put(c.Id, caseVar);
        }
  
        String postJson = formatJSON(JSON.serialize(caseMap.values()));
        system.debug(postJson);  

        // Aneeq ---> TODO: post 'postJson' to webservice        
    }
    
}