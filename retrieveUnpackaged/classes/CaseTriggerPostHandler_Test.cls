/*
	Author : Dharma Penugonda
	Created Date : 08/15/2015
	Purpose : Provides Unit Testing for all Business Logic covered under CaseTriggerPostHandler class
*/

@isTest(SeeAllData=false)
public class CaseTriggerPostHandler_Test {
   
    //Test IdSplit method
    public static TestMethod void IdSplitTest() {
        UtlityTest testHandler = new UtlityTest();
        List<Event> eve = testHandler.createMultiEvtRec(5);
        for(Event e : eve){
            string s = caseTriggerPostHandler.IdSplit(e.id);
            system.assertEquals(15, s.length());           
        }       
    }
    
    //Test FormatJson method
    public static TestMethod void CheckNull() {
        UtlityTest testHandler = new UtlityTest();
        List<Event> eve = testHandler.createMultiEvtRec(5);
        String jsonString = caseTriggerPostHandler.FormatJson(JSON.Serialize(eve));
        system.assertEquals(false, jsonString.containsIgnoreCase('null'));
    }   
    
    //Test case insert logic and make sure no closed cases are passing
    public static TestMethod void CaseInsertTest(){
        UtlityTest testHandler = new UtlityTest();
        User u = testHandler.createTestUsr();
        Contact con = testHandler.createContactRec();
        List<Case> cList = testHandler.createMultiCaseRec(con, u, 10); 
        Test.startTest();
        caseTriggerPostHandler cp = new caseTriggerPostHandler();
        cp.caseInsert(cList);       
        Test.stopTest();
        List<Case> nList = [Select id, owner.profile.name, contactId, status from case where status = 'closed'];
        system.assertEquals(0, nList.size());
        List<Case> nList1 = [Select id, owner.profile.name, contactId, status from case where status = 'open'];
        system.assertNotEquals(0, nList1.size());        
    }
    
    //Test case update logic
    public static TestMethod void CaseUpdateTest(){
        UtlityTest testHandler = new UtlityTest();
        User u = testHandler.createTestUsr();
        Contact con = testHandler.createContactRec();
        List<case> cList = new List<case>();
        Case c = testHandler.createCaseRec(con);
        Map<Id, Case> oMap = new map<Id, Case>();
        oMap.put(c.Id, c);
        Group g = testHandler.createCaseQueue(u, 'test');
        c.OwnerId = g.Id;
        Upsert c;
        
    }    
    
    //Test postCases_User
    public static TestMethod void PostCaseUserTest(){
        List<Id> caseIds = new List<Id>();
        UtlityTest testHandler = new UtlityTest(); 
        User u = testHandler.createTestUsr();
        Account a = testHandler.createAccntRec();
        Account a1 = testHandler.createAccntRec();
        a.parentId = a1.id;
        Upsert a;
        Asset at = new Asset(name = 'test', accountId = a1.id, description = 'TN', serialnumber = '123456789');
        Insert at;
        Case c = testHandler.createCasewtAcnt(a);
        caseIds.add(c.id);
        system.runAs(u) {
            test.startTest();
            caseTriggerPostHandler.postCases_User(caseIds);
            test.stopTest();
        }
    }

    //Test PostCases_Queue (bulk record, make 10 as 1000 to check)
    public static TestMethod void PostCaseQueueTest(){
        List<Id> i = new List<Id>();
        UtlityTest testHandler = new UtlityTest();
        Contact con = testHandler.createContactRec();
        User u = testHandler.createTestUsr();
        Group g = testHandler.createCaseQueue(u, 'test');
        List<Case> cList = testHandler.createMultiCaseRec(con, u, 10);
        for(Case c: cList){
            i.add(c.id);
            c.ownerId = g.id;
        }
        Upsert cList;       
        test.startTest();
        caseTriggerPostHandler.postCases_Queue(i);
        test.stopTest();
        Case caseTest = [SELECT Id, Status, owner.type from Case Limit 1];
        system.assertNotEquals('User', caseTest.owner.type);
    }

    //Test for queue on case Insert
    public static TestMethod void QueueTest(){
        UtlityTest testHandler = new UtlityTest();
        User u = testHandler.createTestUsr();
        Group g = testHandler.createCaseQueue(u, 'test');
        Contact con = testHandler.createContactRec();
        List<Case> cList = testHandler.createMultiCaseRec(con, u, 10); 
        for(case c: cList){
            c.OwnerId = g.id;
        }
        upsert cList;
        test.startTest();
        caseTriggerPostHandler cp = new caseTriggerPostHandler();
        cp.CaseInsert(cList);       
        test.stopTest();
    }    
    
}