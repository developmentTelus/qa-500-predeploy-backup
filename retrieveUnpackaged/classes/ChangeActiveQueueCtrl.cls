// **************************************************************************************************************
// Name:            ChangeActiveQueueCtrl Class
// Description:     Controller for the "Change Active Case Queue" page
// Author(s):       Haydar Hadi
// Created:         May 29, 2015
// ************************Version Updates***********************************************************************
//
// Updated Date     Updated By      Update Comments 
//
// **************************************************************************************************************
public with sharing class ChangeActiveQueueCtrl {

	public final Map<Id, String> membershipQueues {get;set;}

	public Id selectedQueueId {get;set;}
	public Id activeQueueId {get;set;}
	public Id defaultQueueId {get;set;}

	public final String DEFAULT_QUEUE_NAME = 'Mobility - EN';

	//
	// Constructor
	//
	public ChangeActiveQueueCtrl() {

		membershipQueues = queryMembershipQueues();
		activeQueueId = userActiveQueueId();

		if (membershipQueues.size()==0) {
			error(Label.Error_No_Queue_Membership);
			return;
		}

		// if agent does not have an active queue specified
		// or
		// if he is not member of his active queue
		if (activeQueueId==null || !membershipQueues.containsKey(activeQueueId)) {

			// get the Id of the "Mobility - EN" queue
			selectedQueueId = getQueueIdByName(DEFAULT_QUEUE_NAME);
		} else {
			selectedQueueId = activeQueueId;
		}
	}

	// 
	// code behind for "Change Ative Case Button"
	//
	public PageReference updateActiveQueue() {
	
		if (!String.isEmpty(selectedQueueId)) {
			updateUserActiveQueue(selectedQueueId);
			confirm(Label.Confirm_Change_Active_Queue);
		} else {
			warn(Label.Warn_Select_Queue);
		}

		return null;
	}


	public List<SelectOption> getUserQueueSelections() {
		
		List<SelectOption> selections = new List<SelectOption>();

		for (Id queueId: membershipQueues.keySet()) {
			String queueName = membershipQueues.get(queueId);
			selections.add(new SelectOption(queueId, queueName));
		}

		return selections;
	}


	public Map<Id, String> queryMembershipQueues() {

		Set<Id> groupIds = 	getAllGroupIds();

		Map<Id, String> mp = new Map<Id, String>();
		for(QueueSobject q: [
			Select q.Queue.ID, q.Queue.Name
			from QueueSobject q
			where q.Queue.Id in :groupIds
			ORDER BY q.Queue.Name
		]) {
			mp.put(q.Queue.Id, q.Queue.Name);
		}

		return mp;
	}

	private static Set<ID> getAllParentRoles(Id baseRoleID) {

		if (baseRoleID == null) return new Set<Id>();

		Map<Id, UserRole> allRoles = new Map<Id, UserRole>([Select Id, ParentRoleId From UserRole]);

		Set<Id> roleIds = new Set<Id> {baseRoleID};

		Id roleId = baseRoleID; 
		Id parentRoleId;
		Integer c=0;
		do {
			parentRoleId = allRoles.get(roleId).ParentRoleId;
			if (parentRoleId!=null) {
				roleIds.add(parentRoleId);
				roleId = parentRoleId;
			}
		} while (parentRoleId != null && c++<10);

		return roleIds;
	}

	private Set<Id> getAllGroupIds() {

		Id userId = UserInfo.getUserId();
		Id roleId = UserInfo.getUserRoleId();

		Set<Id> parentRoleIds = getAllParentRoles(roleId);

		Map<Id, Group> roleGroups = new Map<Id, Group>([
			Select Id, Type, DeveloperName, RelatedId 
			From Group 
			where 
				(Type like 'RoleAndSubordinates%' and RelatedId in :parentRoleIds)
				or
				(Type = 'Role' and RelatedId = :roleId)
		]);

		Set<Id> workingIds = new Set<Id>();
		workingIds.add(userId);
		workingIds.addAll(roleGroups.keyset());

		Integer size;
		Boolean sizeChanged = true;
		Integer c = 0;
		while (sizeChanged && c++<15) {
			size = workingIds.size();
			for(GroupMember gm: [select Id, groupId, userOrGroupId from GroupMember where userOrGroupId in :workingIds]) {
				workingIds.add(gm.groupId);
			}
			sizeChanged = (size!=workingIds.size());
		}

		return workingIds;
	}



	public void updateUserActiveQueue(Id newQueueId) {
		Active_Queue__c setting = Active_Queue__c.getInstance(UserInfo.getUserId());

		if (setting == null) 
			setting = new Active_Queue__c();

		setting.CaseQueueId__c = newQueueId;
		upsert setting;		
	}

	public Id userActiveQueueId() {
		Active_Queue__c setting = Active_Queue__c.getInstance(UserInfo.getUserId());

		return setting!=null
			? (Id) setting.CaseQueueId__c
			: null;
	}

	public Id getQueueIdByName(String name) {

		for (Id queueId: membershipQueues.keySet()) {
			String queueName = membershipQueues.get(queueId);
			if (queueName == name) return queueId;
		}
		return null;
	}

	////////////////////
	/// Messages - Help Methods
	////////////////////

	private void confirm(String name) {
		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, name));
	}

	private void warn(String name) {
		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, name));
	}

	private void error(String name) {
		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, name));
	}	
}